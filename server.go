package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"go-informe/controllers"
	"go-informe/helpers"
	"go-informe/models"
	"go-informe/repository"
	"go-informe/services"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/gocolly/colly"
	"github.com/gocolly/colly/debug"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/subosito/gotenv"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gopkg.in/mgo.v2/bson"
)

type ScrappingProductTokopedia struct {
	ProductId  string `bson:"product_id"`
	Name       string `bson:"name"`
	Price      string `bson:"price"`
	Rating     string `bson:"rating"`
	Sold       string `bson:"sold"`
	Store      string `bson:"store"`
	UrlProduct string `bson:"url_product"`
}
type StorageObjectId struct {
	ProductId string `bson:"_id"`
}

// func Tokopedia(url string) {
//  c := colly.NewCollector(
//      colly.Debugger(&debug.LogDebugger{}),
//      colly.UserAgent("xy"),
//  )
//  productData := []ScrappingProductTokopedia{}
//  prodData := ScrappingProductTokopedia{}
//  c.OnHTML(".css-o1ozdj", func(e *colly.HTMLElement) {
//      prodData.Name = e.ChildText("div[data-testid=spnSRPProdName]")
//      prodData.Data = e.ChildText("div[data-testid=spnSRPProdPrice]")
//      prodData.Time = time.Now()
//      productData = append(productData, prodData)
//      // Print link
//      fmt.Println("Name : " + prodData.Name)
//      fmt.Println("Price : " + prodData.Data)
//      fmt.Println(strconv.FormatInt(time.Now().Unix(), 10))
//      // Visit link found on page
//      //e.Request.Visit(link)
//      session, err := helpers.ConnectMongoDB()
//      if err != nil {
//          fmt.Println("Cannot connect to MongoDB")
//      }
//      defer session.Close()
//      var collection = session.DB("scrapping").C("spesificationProduct")
//      collection.Insert(productData)
//  })

//  c.Visit(url)
// }

func scrapTokoped(url string) {
	c := colly.NewCollector(
		colly.Debugger(&debug.LogDebugger{}),
		colly.UserAgent("xy"),
	)
	productData := []ScrappingProductTokopedia{}
	prodData := ScrappingProductTokopedia{}
	c.OnHTML(".css-7fmtuv", func(e *colly.HTMLElement) {
		prodData.Name = e.ChildText("div[data-testid=spnSRPProdName]")
		prodData.Price = e.ChildText("div[data-testid=spnSRPProdPrice]")
		prodData.Rating = e.ChildText(".css-etd83i")
		prodData.Sold = e.ChildText(".css-1mv2cn2")
		prodData.ProductId = "101011"
		prodData.Store = e.ChildText("span[data-testid=spnSRPProdTabShopLoc]")
		prodData.UrlProduct = e.ChildAttr("a", "href")
		productData = append(productData, prodData)
		a, _ := json.Marshal(prodData)
		fmt.Println(string(a))

		// Print link
		connect := new(models.DatabaseMongo)
		ctx := context.Background()
		session, err := connect.ConnectMongoDB()
		if err != nil {
			fmt.Println("Cannot connect to MongoDB")
		}
		defer session.Disconnect(ctx)
		var collection = session.Database("product").Collection("ecommerce_data")
		collection.InsertOne(ctx, prodData)
	})

	c.Visit(url)
}

func scrapIPrice(url string) {
	c := colly.NewCollector(
		colly.Debugger(&debug.LogDebugger{}),
		colly.UserAgent("xy"),
	)
	temp := models.SpesificationProductGsmArena{}
	c.OnHTML(".summarySpec", func(e *colly.HTMLElement) {
		data, _ := e.DOM.Find(".specTbl").Html()
		temp.Spesification = data
		// a, _ := json.Marshal(temp)
		fmt.Print("ini loh " + data)

		// fmt.Print(data)
		// // Print link
		// connect := new(models.DatabaseMongo)
		// ctx := context.Background()
		// session, err := connect.ConnectMongoDB()
		// if err != nil {
		//  fmt.Println("Cannot connect to MongoDB")
		// }
		// defer session.Disconnect(ctx)
		// var collection = session.Database("product").Collection("ecommerce_data")
		// collection.InsertOne(ctx, prodData)
	})
	c.OnHTML(".itemSumName", func(b *colly.HTMLElement) {
		fmt.Print("masuk masuk")
		nameProduct := b.ChildText(".ttl")
		temp.Title = nameProduct
	})
	c.OnHTML(".itemSumImg > .mainImg", func(d *colly.HTMLElement) {
		urlPhoto := d.ChildAttr("#mainItemImg", "src")
		temp.Photo = urlPhoto
	})
	c.OnHTML(".itemList", func(a *colly.HTMLElement) {

	})
	c.OnHTML(".itmName", func(t *colly.HTMLElement) {
		link := t.Attr("href")
		fmt.Print("in link dari class itemname", link)

	})
	c.OnHTML("a[href]", func(h *colly.HTMLElement) {
		link := h.Attr("href")
		fmt.Println("Visit Link", link)
		h.Request.Visit("https://id.priceprice.com/harga-laptop/" + link)
	})
	c.Visit(url)
}

// func scrapGsmArena(url string) {
//  c := colly.NewCollector(colly.Debugger(&debug.LogDebugger{}), colly.UserAgent("xy"))
//  specPhone := []SpesificationProduct{}
//  temp := SpesificationProduct{}
//  c.OnHTML(".review-header", func(e *colly.HTMLElement) {
//      temp.Title = e.ChildText(".specs-phone-name-title")
//  })
//  c.OnHTML("#specs-list", func(e *colly.HTMLElement) {
//      // helpers.WriteToLogfile("resp.log", e.Text)
//      temp.Spesification = e.Text
//      specPhone = append(specPhone, temp)
//      jsonSpec, _ := json.Marshal(specPhone)
//      fmt.Println(string(jsonSpec))
//      connect := new(models.DatabaseMongo)
//      ctx := context.Background()
//      session, err := connect.ConnectMongoDB()
//      if err != nil {
//          fmt.Println("Cannot connect to MongoDB")
//      }
//      defer session.Disconnect(ctx)
//      var collection = session.Database("product").Collection("spesificationProduct")
//      collection.InsertOne(ctx, temp)
//  })
//  c.Visit(url)
// }

func ScrapGsmArena(url string) {
	c := colly.NewCollector(colly.Debugger(&debug.LogDebugger{}), colly.UserAgent("xy"), colly.CacheDir("./cache"))
	specPhone := []models.SpesificationProductGsmArena{}
	temp := models.SpesificationProductGsmArena{}

	c.OnHTML(".review-header", func(e *colly.HTMLElement) {
		randNumber := rand.Intn(1000)
		randNumberToString := strconv.Itoa(randNumber)
		temp.ProductId = "10" + randNumberToString
		fmt.Println(temp.ProductId)
		temp.Title = e.ChildText(".specs-phone-name-title")

		if strings.Contains(temp.Title, "Samsung") {
			temp.Brands = "Samsung"
		}
		if strings.Contains(temp.Title, "Apple") {
			temp.Brands = "Apple"
		}
		if strings.Contains(temp.Title, "Xiaomi") {
			temp.Brands = "Xiaomi"
		}
		if strings.Contains(temp.Title, "Asus") {
			temp.Brands = "Asus"
		}
		if strings.Contains(temp.Title, "Panasonic") {
			temp.Brands = "Panasonic"
		}
		if strings.Contains(temp.Title, "Huawei") {
			temp.Brands = "Huawei"
		}
		if strings.Contains(temp.Title, "Nokia") {
			temp.Brands = "Nokia"
		}
		if strings.Contains(temp.Title, "Sony") {
			temp.Brands = "Sony"
		}
		if strings.Contains(temp.Title, "LG") {
			temp.Brands = "LG"
		}
		if strings.Contains(temp.Title, "Motorola") {
			temp.Brands = "Motorola"
		}
		if strings.Contains(temp.Title, "Lenovo") {
			temp.Brands = "Lenovo"
		}
		if strings.Contains(temp.Title, "Google") {
			temp.Brands = "Google"
		}
		if strings.Contains(temp.Title, "Honor") {
			temp.Brands = "Honor"
		}
		if strings.Contains(temp.Title, "Oppo") {
			temp.Brands = "Oppo"
		}
		if strings.Contains(temp.Title, "Realme") {
			temp.Brands = "Realme"
		}
		if strings.Contains(temp.Title, "Oneplus") {
			temp.Brands = "Oneplus"
		}
		if strings.Contains(temp.Title, "Vivo") {
			temp.Brands = "Vivo"
		}
		if strings.Contains(temp.Title, "Meizu") {
			temp.Brands = "Meizu"
		}
		if strings.Contains(temp.Title, "Blackberry") {
			temp.Brands = "Blackberry"
		}
		if strings.Contains(temp.Title, "Alcatel") {
			temp.Brands = "Alcatel"
		}
		if strings.Contains(temp.Title, "ZTE") {
			temp.Brands = "ZTE"
		}
		if strings.Contains(temp.Title, "Microsoft") {
			temp.Brands = "Microsoft"
		}
		if strings.Contains(temp.Title, "Vodafone") {
			temp.Brands = "Vodafone"
		}
		if strings.Contains(temp.Title, "Energizer") {
			temp.Brands = "Energizer"
		}
		if strings.Contains(temp.Title, "CAT") {
			temp.Brands = "CAT"
		}
		if strings.Contains(temp.Title, "Sharp") {
			temp.Brands = "Sharp"
		}
		if strings.Contains(temp.Title, "Micromax") {
			temp.Brands = "Micromax"
		}
		if strings.Contains(temp.Title, "Infinix") {
			temp.Brands = "Infinix"
		}
		if strings.Contains(temp.Title, "Ulefone") {
			temp.Brands = "Ulefone"
		}
		if strings.Contains(temp.Title, "Tecno") {
			temp.Brands = "Tecno"
		}
		if strings.Contains(temp.Title, "Blu") {
			temp.Brands = "Blu"
		}
		if strings.Contains(temp.Title, "Acer") {
			temp.Brands = "Acer"
		}
		if strings.Contains(temp.Title, "Wiko") {
			temp.Brands = "Wiko"
		}
		if strings.Contains(temp.Title, "Verykool") {
			temp.Brands = "Verykool"
		}
		if strings.Contains(temp.Title, "Plum") {
			temp.Brands = "Plum"
		}
		if strings.Contains(temp.Title, "Htc") {
			temp.Brands = "Htc"
		}
	})
	c.OnHTML(".specs-photo-main > a > img", func(e *colly.HTMLElement) {
		temp.Photo = e.Attr("src")
	})

	c.OnHTML("#specs-list", func(e *colly.HTMLElement) {
		// helpers.WriteToLogfile("resp.log", e.Text)
		checkProductId := true
		checkTitle := true
		checkProductId = CheckIfProductIdExist(temp.ProductId)
		checkTitle = CheckIfTitleExist(temp.Title)
		temp.Spesification, _ = e.DOM.Html()
		temp.Category = "Handphone"
		specPhone = append(specPhone, temp)
		jsonSpec, _ := json.Marshal(specPhone)
		fmt.Println(string(jsonSpec))
		if checkProductId != true && checkTitle != true {
			connect := new(models.DatabaseMongo)
			ctx := context.Background()
			session, err := connect.ConnectMongoDB()
			if err != nil {
				fmt.Println("Cannot connect to MongoDB")
			}
			defer session.Disconnect(ctx)
			var collection = session.Database("product").Collection("spesificationProduct")
			collection.InsertOne(ctx, temp)
		}
	})
	c.OnHTML("a[href]", func(h *colly.HTMLElement) {
		if h.Attr("class") == "makers" {
			return
		}
		link := h.Attr("href")
		fmt.Println(link)
		if !strings.HasSuffix(link, ".php") {
			return
		}
		h.Request.Visit(link)
	})
	c.OnHTML(".brandmenu-v2.light.l-box.clearfix > ul > li > a[href]", func(t *colly.HTMLElement) {
		fmt.Print("woi")
		link := t.Attr("href")
		fmt.Println("https://www.gsmarena.com/" + link)
		if !strings.HasSuffix(link, ".php") {
			return
		}
		t.Request.Visit("https://www.gsmarena.com/" + link)
	})
	c.Limit(&colly.LimitRule{
		// DomainRegexp: "127.0.0.1:8000",
		// Parallelism:  2,
		Delay: 5 * time.Second,
	})
	// Rotate two socks5 proxies
	// rp, err := proxy.RoundRobinProxySwitcher("socks5://127.0.0.1:1000", "socks5://127.0.0.1:2500")
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// c.SetProxyFunc(rp)

	c.Visit(url)
}

func ScrapLaptopArena(url string) {
	c := colly.NewCollector(
		colly.Debugger(&debug.LogDebugger{}),
		colly.UserAgent("xy"),
	)
	specLaptop := []models.SpesificationProductGsmArena{}
	temp := models.SpesificationProductGsmArena{}
	// temp := models.SpesificationProductGsmArena{}
	c.OnHTML(".grid-x > .large-3.medium-6.small-12.cell.index_product", func(e *colly.HTMLElement) {
		link := e.ChildAttr("a", "href")
		e.Request.Visit("https://www.laptoparena.net" + link)
	})
	c.OnHTML("a[href]", func(h *colly.HTMLElement) {
		if h.Attr("class") == "makers" {
			return
		}
		link := h.Attr("href")
		fmt.Println(link)
		if !strings.HasSuffix(link, ".php") {
			return
		}
		h.Request.Visit("https://www.laptoparena.net/" + link)
	})
	c.OnHTML(".laptop_h1", func(e *colly.HTMLElement) {
		temp.Title = e.Text
	})
	c.OnHTML(".grid-x", func(e *colly.HTMLElement) {
		specsLaptop, _ := e.DOM.Find(".specs.responsive").Html()
		randNumber := rand.Intn(1000)
		randNumberToString := strconv.Itoa(randNumber)
		productId := "20" + randNumberToString
		temp.ProductId = productId
		temp.Spesification = specsLaptop
		urlPhoto := e.ChildAttr(".gallery-image", "src")
		temp.Photo = "https://www.laptoparena.net" + urlPhoto
		temp.Category = "Laptop"
		fmt.Println("url photo ini " + urlPhoto)
		specLaptop = append(specLaptop, temp)
		checkProductId := true
		checkTitle := true
		checkProductId = CheckIfProductIdExist(temp.ProductId)
		checkTitle = CheckIfTitleExist(temp.Title)
		if temp.Title != "" && temp.Spesification != "" && temp.Photo != "" && checkProductId != true && checkTitle != true {
			jsonSpec, _ := json.Marshal(specLaptop)
			fmt.Println(string(jsonSpec))
			connect := new(models.DatabaseMongo)
			ctx := context.Background()
			session, err := connect.ConnectMongoDB()
			if err != nil {
				fmt.Println("Cannot connect to MongoDB")
			}
			defer session.Disconnect(ctx)
			var collection = session.Database("product").Collection("spesificationProduct")
			collection.InsertOne(ctx, temp)
		}
	})
	c.Visit(url)
}
func CheckIfProductIdExist(productId string) bool {
	connect := new(models.DatabaseMongo)
	data := []models.SpesificationProductGsmArena{}
	ctx := context.TODO()
	session, err := connect.ConnectMongoDB()

	if err != nil {
		fmt.Println("Cannot connect to MongoDB")
	}
	defer session.Disconnect(ctx)
	var collection = session.Database("product").Collection("spesificationProduct")
	find, err := collection.Find(ctx, bson.M{"product_id": productId}, options.Find())

	err = find.All(ctx, &data)
	if len(data) != 0 {
		return true
	}
	return false
}
func CheckIfTitleExist(title string) bool {
	connect := new(models.DatabaseMongo)
	data := []models.SpesificationProductGsmArena{}
	ctx := context.TODO()
	session, err := connect.ConnectMongoDB()

	if err != nil {
		fmt.Println("Cannot connect to MongoDB")
	}
	defer session.Disconnect(ctx)
	var collection = session.Database("product").Collection("spesificationProduct")
	find, err := collection.Find(ctx, bson.M{"title": title}, options.Find())

	err = find.All(ctx, &data)
	if len(data) != 0 {
		return true
	}
	return false
}

// func GetAllDataFromMongoDB() []models.SpesificationProductGsmArena {
// 	connect := new(models.DatabaseMongo)
// 	data := []models.SpesificationProductGsmArena{}
// 	ctx := context.TODO()
// 	session, err := connect.ConnectMongoDB()

// 	if err != nil {
// 		fmt.Println("Cannot connect to MongoDB")
// 	}
// 	defer session.Disconnect(ctx)
// 	var collection = session.Database("product").Collection("spesificationProduct")
// 	find, err := collection.Find(ctx, bson.M{})

// 	err = find.All(ctx, &data)
// 	return data
// }
// func ConvertScrapToJsonPhone() {
// 	ab := GetAllDataFromMongoDB()
// 	arrData := []models.PhoneData{}
// 	data := models.PhoneData{}
// 	objectId := []StorageObjectId{}
// 	for i := 0; i < len(ab); i++ {
// 		// dataSpec := strings.Split(ab[i].Spesification, "\n")

// 		a := ab[i].Spesification
// 		// fmt.Println(a)
// 		// b := strings.Index(a, "Sound")
// 		// fmt.Println(b)
// 		// ca := strings.TrimSpace(ab[i].Spesification)
// 		// b := strings.ReplaceAll(ca, "Network", "|||")
// 		// c := strings.Index(ab[i].Spesification, strings.Contains("newt"))

// 		//for row network

// 		randNumber := rand.Intn(1000)
// 		randNumberToString := strconv.Itoa(randNumber)
// 		sku := "10" + randNumberToString
// 		fmt.Print(sku)

// 		data.Title = ab[i].Title
// 		data.UrlPhoto = ab[i].Photo
// 		offset, length := helpers.GetIndexTwoString(a, "Network", "Launch")
// 		network := a[offset:length]
// 		offset, length = helpers.GetIndexTwoString(network, "Technology", "2G bands")
// 		data.Network.Technology = strings.Trim(strings.Trim(network[offset:length], "Technology"), "\n\t")
// 		offset, length = helpers.GetIndexTwoString(network, "2G bands", "3G bands")
// 		data.Network.TwoGBands = strings.Trim(strings.Trim(network[offset:length], "2G bands"), "\n\t")
// 		offset, length = helpers.GetIndexTwoString(network, "2G bands", "3G bands")
// 		data.Network.TwoGBands = strings.Trim(strings.Trim(network[offset:length], "2G bands"), "\n\t")
// 		offset, length = helpers.GetIndexTwoString(network, "3G bands", "4G bands")
// 		data.Network.ThreeGBands = strings.Trim(strings.Trim(network[offset:length], "3G bands"), "\n\t")
// 		offset, length = helpers.GetIndexTwoString(network, "4G bands", "Speed")
// 		data.Network.FourGBands = strings.Trim(strings.Trim(network[offset:length], "4G bands"), "\n\t")
// 		positionSpeed := strings.Index(network, "Speed")
// 		data.Network.Speed = strings.Trim(strings.Trim(network[positionSpeed:], "Speed"), "\n\t")
// 		// fmt.Println(data.Network.Speed)
// 		//end for row network, continue ...
// 		// ac := strings.Trim(strings.Trim(data.Network.Technology, "Network")

// 		//for row launch
// 		offset, length = helpers.GetIndexTwoString(a, "Launch", "Body")
// 		launch := a[offset:length]
// 		offset, length = helpers.GetIndexTwoString(launch, "Announced", "Status")
// 		data.Launch.Announced = strings.Trim(strings.Trim(launch[offset:length], "Announced"), "\n\t")
// 		positionStatus := strings.Index(launch, "Status")
// 		data.Launch.Status = strings.Trim(strings.Trim(launch[positionStatus:], "Status"), "\n\t")

// 		//end for row launch, continue ...

// 		//for row body
// 		offset, length = helpers.GetIndexTwoString(a, "Body", "Display")
// 		body := a[offset:length]
// 		offset, length = helpers.GetIndexTwoString(body, "Dimensions", "Weight")
// 		data.Body.Dimensions = strings.Trim(strings.Trim(body[offset:length], "Dimensions"), "\n\t")
// 		offset, length = helpers.GetIndexTwoString(body, "Weight", "Build")
// 		data.Body.Weight = strings.Trim(strings.Trim(body[offset:length], "Weight"), "\n\t")
// 		offset, length = helpers.GetIndexTwoString(body, "Build", "SIM")
// 		data.Body.Build = strings.Trim(strings.Trim(body[offset:length], "Build"), "\n\t")
// 		positionSim := strings.Index(body, "SIM")
// 		data.Body.Sim = strings.Trim(strings.Trim(body[positionSim:], "SIM"), "\n\t")
// 		//end for row body, continue ..

// 		//for row Display
// 		offset, length = helpers.GetIndexTwoString(a, "Display", "Platform")
// 		display := a[offset:length]
// 		offset, length = helpers.GetIndexTwoString(display, "Type", "Size")
// 		data.Display.Type = strings.Trim(strings.Trim(display[offset:length], "Type"), "\n\t")
// 		offset, length = helpers.GetIndexTwoString(display, "Size", "Resolution")
// 		data.Display.Size = strings.Trim(strings.Trim(display[offset:length], "Size"), "\n\t")
// 		positionResolution := strings.Index(display, "Resolution")
// 		data.Display.Resolution = strings.Trim(strings.Trim(display[positionResolution:], "Resolution"), "\n\t")
// 		//end for row display, continue ...

// 		//for row platform
// 		offset, length = helpers.GetIndexTwoString(a, "Platform", "Memory")
// 		platform := a[offset:length]
// 		offset, length = helpers.GetIndexTwoString(platform, "OS", "Chipset")
// 		data.Platform.Os = strings.Trim(strings.Trim(platform[offset:length], "OS"), "\n\t")
// 		offset, length = helpers.GetIndexTwoString(platform, "Chipset", "CPU")
// 		data.Platform.Chipset = strings.Trim(strings.Trim(platform[offset:length], "Chipset"), "\n\t")
// 		offset, length = helpers.GetIndexTwoString(platform, "CPU", "GPU")
// 		data.Platform.Cpu = strings.Trim(strings.Trim(platform[offset:length], "CPU"), "\n\t")
// 		positionGPU := strings.Index(platform, "GPU")
// 		data.Platform.Gpu = strings.Trim(strings.Trim(platform[positionGPU:], "GPU"), "\n\t")
// 		//end for row platform

// 		// for row Memory
// 		offset, length = helpers.GetIndexTwoString(a, "Memory", "Main Camera")
// 		memory := a[offset:length]
// 		offset, length = helpers.GetIndexTwoString(memory, "Card slot", "Internal")
// 		data.Memory.CardSlot = strings.Trim(strings.Trim(memory[offset:length], "Card slot"), "\n\t")
// 		positionInternal := strings.Index(memory, "Internal")
// 		data.Memory.Internal = strings.Trim(strings.Trim(memory[positionInternal:], "Internal"), "\n\t")
// 		//end for row Memory, continue ...

// 		// for row Main Camera
// 		offset, length = helpers.GetIndexTwoString(a, "Main Camera", "Selfie camera")
// 		mainCamera := a[offset:length]
// 		if strings.Contains(mainCamera, "Triple") == true {
// 			offset, length = helpers.GetIndexTwoString(mainCamera, "Triple", "Features")
// 			data.MainCamera.Triple = strings.Trim(strings.Trim(mainCamera[offset:length], "Triple"), "\n\t")
// 		}
// 		if strings.Contains(mainCamera, "Quad") == true {
// 			offset, length = helpers.GetIndexTwoString(mainCamera, "Quad", "Features")
// 			data.MainCamera.Quad = strings.Trim(strings.Trim(mainCamera[offset:length], "Quad"), "\n\t")
// 		}
// 		offset, length = helpers.GetIndexTwoString(mainCamera, "Features", "Video")
// 		data.MainCamera.Features = strings.Trim(strings.Trim(mainCamera[offset:length], "Features"), "\n\t")
// 		positionVideo := strings.Index(mainCamera, "Video")
// 		data.MainCamera.Video = strings.Trim(strings.Trim(mainCamera[positionVideo:], "Video"), "\n\t")
// 		//end for row Main Camera, continue ...

// 		//for row Selfie Camera

// 		offset, length = helpers.GetIndexTwoString(a, "Selfie camera", "Sound")
// 		selfieCamera := a[offset:length]
// 		if strings.Contains(selfieCamera, "Single") == true {
// 			offset, length = helpers.GetIndexTwoString(selfieCamera, "Single", "Features")
// 			data.SelfieCamera.Single = strings.Trim(strings.Trim(selfieCamera[offset:length], "Single"), "\n\t")
// 		}
// 		offset, length = helpers.GetIndexTwoString(selfieCamera, "Features", "Video")
// 		data.SelfieCamera.Features = strings.Trim(strings.Trim(selfieCamera[offset:length], "Features"), "\n\t")
// 		positionVideos := strings.Index(selfieCamera, "Video")
// 		data.SelfieCamera.Video = strings.Trim(strings.Trim(selfieCamera[positionVideos:], "Video"), "\n\t")

// 		// end for row Selfie Camera, continue...

// 		//for row Sound
// 		offset, length = helpers.GetIndexTwoString(a, "Sound", "Comms")
// 		sound := a[offset:length]
// 		offset, length = helpers.GetIndexTwoString(sound, "Loudspeaker", "3.5mm jack")
// 		data.Sound.Loudspeaker = strings.Trim(strings.Trim(sound[offset:length], "Loudspeaker"), "\n\t")
// 		positionJack := strings.Index(sound, "3.5mm jack")
// 		data.Sound.IsJack = strings.Trim(strings.Trim(sound[positionJack:], "3.5mm jack"), "\n\t")
// 		//end for row sound, continue ...

// 		// mixed row from comms before misc
// 		// for data row comms
// 		offset, length = helpers.GetIndexTwoString(a, "Comms", "Misc")
// 		mixedData := a[offset:length]
// 		offset, length = helpers.GetIndexTwoString(mixedData, "WLAN", "Bluetooth")
// 		data.Comms.Wlan = strings.Trim(strings.Trim(mixedData[offset:length], "WLAN"), "\n\t")
// 		offset, length = helpers.GetIndexTwoString(mixedData, "Bluetooth", "GPS")
// 		data.Comms.Bluetooth = strings.Trim(strings.Trim(mixedData[offset:length], "Bluetooth"), "\n\t")
// 		offset, length = helpers.GetIndexTwoString(mixedData, "GPS", "NFC")
// 		data.Comms.Gps = strings.Trim(strings.Trim(mixedData[offset:length], "GPS"), "\n\t")
// 		offset, length = helpers.GetIndexTwoString(mixedData, "NFC", "Radio")
// 		data.Comms.Nfc = strings.Trim(strings.Trim(mixedData[offset:length], "NFC"), "\n\t")
// 		offset, length = helpers.GetIndexTwoString(mixedData, "Radio", "USB")
// 		data.Comms.Radio = strings.Trim(strings.Trim(mixedData[offset:length], "Radio"), "\n\t")
// 		offset, length = helpers.GetIndexTwoString(mixedData, "USB", "Features")
// 		data.Comms.Radio = strings.Trim(strings.Trim(mixedData[offset:length], "USB"), "\n\t")
// 		//end data row comms

// 		//for data row Features
// 		offset, length = helpers.GetIndexTwoString(mixedData, "Sensors", "Battery")
// 		data.Features.Sensors = strings.Trim(strings.Trim(mixedData[offset:length], "Sensors"), "\n\t")
// 		//end data row features, continue ...

// 		//for data row battery
// 		offset, length = helpers.GetIndexTwoString(mixedData, "Type", "Charging")
// 		data.Battery.Type = strings.Trim(strings.Trim(mixedData[offset:length], "Type"), "\n\t")
// 		positionChargingstr := strings.Index(mixedData, "Charging")
// 		data.Battery.Charging = strings.Trim(strings.Trim(mixedData[positionChargingstr:], "Charging"), "\n\t")

// 		//end data row misc.

// 		//for data row misc
// 		positionMiscstr := strings.Index(a, "Misc")
// 		miscData := a[positionMiscstr:]
// 		offset, length = helpers.GetIndexTwoString(miscData, "Colors", "Models")
// 		data.Misc.Colors = strings.Trim(strings.Trim(miscData[offset:length], "Colors"), "\n\t")
// 		if strings.Contains(miscData, "SAR EU") == true {
// 			offset, length = helpers.GetIndexTwoString(miscData, "Models", "SAR EU")
// 			data.Misc.Models = strings.Trim(strings.Trim(miscData[offset:length], "Models"), "\n\t")
// 			offset, length = helpers.GetIndexTwoString(miscData, "SAR EU", "Price")
// 			data.Misc.SarEU = strings.Trim(strings.Trim(miscData[offset:length], "SAR EU"), "\n\t")
// 		} else {
// 			offset, length = helpers.GetIndexTwoString(miscData, "Models", "Price")
// 			data.Misc.Models = strings.Trim(strings.Trim(miscData[offset:length], "Models"), "\n\t")
// 		}
// 		positionPriceStr := strings.Index(miscData, "Price")
// 		data.Misc.Price = strings.Trim(strings.Trim(miscData[positionPriceStr:], "Price"), "\n\t")

// 		arrData = append(arrData, data)
// 		objectId = append(objectId, StorageObjectId{ProductId: ab[i].Id})

// 	}
// 	// //harus divalidasi
// 	// offset, length = helpers.GetIndexTwoString(a, "Features", "Battery")
// 	// data.Features = a[offset:length]
// 	// offset, length = helpers.GetIndexTwoString(a, "Battery", "Misc")
// 	// data.Battery = a[offset:length]
// 	// offsetMisc := strings.Index(a, "Misc")
// 	// data.Misc = a[offsetMisc:]
// 	// fmt.Println(ac)
// 	// fmt.Println(data.Network)

// 	// ad, _ := json.Marshal(arrData)
// 	// fmt.Println(string(ad))

// 	if len(arrData) != 0 {
// 		for i := 0; i < len(arrData); i++ {
// 			ctx := context.Background()
// 			connect := new(models.DatabaseMongo)
// 			sess, err := connect.ConnectMongoDB()
// 			if err != nil {
// 				fmt.Println("Cannot connect to MongoDB")
// 			}
// 			defer sess.Disconnect(ctx)
// 			context := context.Background()
// 			var collection = sess.Database("product").Collection("real_spesificationProduct")
// 			collection.InsertOne(context, arrData[i])
// 		}
// 		for j := 0; j < len(objectId); j++ {
// 			ctx := context.Background()
// 			connect := new(models.DatabaseMongo)
// 			sess, err := connect.ConnectMongoDB()
// 			if err != nil {
// 				fmt.Println("Cannot connect to MongoDB")
// 			}
// 			defer sess.Disconnect(ctx)
// 			context := context.Background()
// 			var collection = sess.Database("product").Collection("spesificationProduct")

// 			collection.DeleteOne(context, bson.M{"_id": objectId[j]})
// 		}
// 	}

// }
func main() {

	flag.Parse()
	// ScrapGsmArena("https://www.gsmarena.com/")
	// scrapIPrice("https://id.priceprice.com/harga-laptop/")
	ScrapLaptopArena("https://www.laptoparena.net/")
	// ConvertScrapToJsonPhone()
	gotenv.Load()
	e := echo.New()
	repo := repository.NewRepository()
	service := services.NewService(repo)
	controllers.NewController(e, service)
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(helpers.LogRequestParam())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE},
	}))
	e.Logger.Fatal(e.Start(":" + os.Getenv("APP_PORT")))

	// e.GET("/login", controllers.Login)
}

// // if err := c.SetStorage(storage); err != nil {
// //   panic(err)
// // }

// // // Find and visit all links
// // c.OnHTML("a[href]", func(e *colly.HTMLElement) {
// //   e.Request.Visit(e.Attr("href"))
// // })

// // c.OnRequest(func(r *colly.Request) {
// //   fmt.Println("Visiting", r.URL)
// // })

// // c.Visit("http://go-colly.org/")
// // // Find and visit all links

// // func main()
// // c := colly.NewCollector(colly.Debugger(&debug.LogDebugger{}), colly.UserAgent("xy"))
// //   // storage := &mongo.Storage{
// //   //  Database: "scrapping",
// //   //  URI:      "mongodb://127.0.0.1:27017",
// //   // }
// //   // c.SetStorage(storage)
// //   specPhone := []SpesificationProduct{}
// //   temp := SpesificationProduct{}
// //   c.OnHTML(".review-header", func(e *colly.HTMLElement) {
// //       // e.Request.Visit(e.Attr("td"))
// //       // comments := make([]*{}, 0, 1000)
// //       // a := &SpesificationProduct{
// //       //  Title: e.ChildText(),
// //       // }

// //       temp.Title = e.ChildText(".specs-phone-name-title")

// //       // specPhone = append(specPhone, temp)
// //       // enc := json.NewEncoder(os.Stdout)
// //       // enc.Encode(specPhone)
// //       // Data := e.ChildAttrs(".techspecs-column", "p[techspecs-subheader]")
// //       // for i := 0; i < len(Data); i++ {
// //       //  fmt.Println(Data[i])
// //       // }
// //   })
// //   c.OnHTML("#specs-list", func(e *colly.HTMLElement) {
// //       WriteToLogfile("resp.log", e.Text)
// //       //var aaa SpesificationProduct
// //       // comments := make([]interface{}, 0, 1000)
// //       // spesific := SpesificationProduct{}
// //       // a := &SpesificationProduct{

// //       //  Spesification: e.Text,
// //       // }
// //       fmt.Println(e.DOM.Find("style"))
// //       temp.Spesification = e.Text
// //       specPhone = append(specPhone, temp)
// //       jsonSpec, _ := json.Marshal(specPhone)
// //       fmt.Println(string(jsonSpec))

// //       // e.Unmarshal(spesific)
// //       // comments = append(comments, a)
// //       // enc := json.NewEncoder(os.Stdout)
// //       // enc.Encode(comments)

// //       //JsonData, _ := json.Marshal(comments)
// //       // fmt.Println(string(JsonData))
// //       session, err := connectMongoDB()
// //       if err != nil {
// //           fmt.Println("Cannot connect to MongoDB")
// //       }
// //       defer session.Close()
// //       var collection = session.DB("scrapping").C("spesificationProduct")
// //       collection.Insert(temp)
// //   })
// //   c.Visit("https://www.gsmarena.com/sony_xperia_5_ii-10396.php")
// //   fmt.Println(len(specPhone))
// //   // // getData := make([]Data, 0)
// //   // collector := colly.NewCollector(
// //   //  colly.AllowedDomains("https://www.apple.com/id/iphone-11/specs/", "www.apple.com/id/iphone-11/specs/"),
// //   // )
// //   // collector.OnHTML("#techspecs with-1-column with-fullwidthrowheader-small", func(element *colly.HTMLElement) {
// //   //  Role := element.Attr("role")
// //   //  factDesc := element.Text

// //   //  a := Data{
// //   //      Role:        Role,
// //   //      Description: factDesc,
// //   //  }
// //   //  getData = append(getData, a)
// //   // })
// //   // collector.OnRequest(func(request *colly.Request) {
// //   //  fmt.Println("Visiting", request.URL.String())
// //   // })
// //   // collector.Visit("https://www.apple.com/id/iphone-11/specs/")
// //   //  enc := json.NewDecoder(os.Stdout)
// //   // enc.setIndent("", " ")
// //   // enc.Encode(getData)
