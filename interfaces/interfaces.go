package interfaces

import (
	"database/sql"
	"go-informe/models"
)

// Repository : Repository interface
type Repository interface {

	//Products
	GetCategoryProduct() interface{}
	InsertComment(c *models.Comment, conn *sql.DB) interface{}
	InsertReplyComment(c *models.ReplyComment, conn *sql.DB) interface{}
	InsertReviewProduct(r *models.Review, conn *sql.DB) interface{}
	GetParentsCommentByProductID(productId, limit, offset string, conn *sql.DB) models.GetAllCommentJSON
	GetTotalDataComment(productId string, conn *sql.DB) models.GetCountData
	GetRepliesCommentByProductId(productId string, conn *sql.DB) []models.GetComment
	GetAllReviewByPorductID(params models.RequestGetAllReview, conn *sql.DB) interface{}
	GetAllReplyCommentByProductID(productId string, conn *sql.DB) interface{}
	GetAllDataProduct() []models.ScrapNewData
	GetEcommerceData(reqParams *models.ReqDataToEcommerce) []models.GetEcommerceData
	SearchNavbarProduct(keyword string) []models.ScrapNewData
	GetListDataByCategory(reqParams *models.ReqListDataByCategory) interface{}
	GetRecommendUpgradeProduct(reqParams *models.ReqUpgradeProduct) []models.PhoneData
	GetProductDetail(keyword string) []models.ScrapNewData
	DeleteComment(reqParams *models.DeleteCommentParam) interface{}
	UpgradeProduct(reqParams *models.UpgradeProductParam) (interface{}, error)
	GetScrappingEcommerce(reqBody *models.ScrappingEcommerceParam) error
	GetPopularProduct() (interface{}, error)
	ScrapAllEcommerce(reqBody *models.ScrapAllEcommerceParam) error
	GetProductPeopleChoose(reqBody *models.ProductPeopleChooseParam) (interface{}, error)
	//User
	Register(u *models.User, conn *sql.DB) error
	GetAuthToken(u *models.User) (string, error)
	IsAuthenticated(u *models.User, conn *sql.DB) (string, string, string, string, error)
	GetPersonalInformationUser(userId string, conn *sql.DB) (interface{}, error)
	UpdateUserInformation(userId string, conn *sql.DB) interface{}
	InsertWishListUser(wishlist *models.StorageWishListUser, conn *sql.DB) error
	GetWishListUser(userId string, conn *sql.DB) (interface{}, error)
	DeleteWishListUser(userId, product_id string, conn *sql.DB) (interface{}, error)
	EditProfile(reqParam *models.EditProfileParam, conn *sql.DB) interface{}
}

// Service : Service interface
type Service interface {

	//Product
	GetCategoryProduct() interface{}
	InsertComment(c *models.Comment, conn *sql.DB) interface{}
	InsertReplyComment(c *models.ReplyComment, conn *sql.DB) interface{}
	InsertReviewProduct(r *models.Review, conn *sql.DB) interface{}
	GetParentsCommentByProductID(productId, limit, offset string, conn *sql.DB) models.GetAllCommentJSON
	GetTotalDataComment(productId string, conn *sql.DB) models.GetCountData
	GetRepliesCommentByProductId(productId string, conn *sql.DB) []models.GetComment
	GetAllReviewByPorductID(params models.RequestGetAllReview, conn *sql.DB) interface{}
	GetAllReplyCommentByProductID(productId string, conn *sql.DB) interface{}
	GetAllDataProduct() []models.ScrapNewData
	GetEcommerceData(reqParams *models.ReqDataToEcommerce) []models.GetEcommerceData
	SearchNavbarProduct(keyword string) []models.ScrapNewData
	GetProductDetail(keyword string) []models.ScrapNewData
	GetListDataByCategory(reqParams *models.ReqListDataByCategory) interface{}
	GetRecommendUpgradeProduct(reqParams *models.ReqUpgradeProduct) []models.PhoneData
	DeleteComment(reqParams *models.DeleteCommentParam) interface{}
	UpgradeProduct(reqParams *models.UpgradeProductParam) (interface{}, error)
	GetScrappingEcommerce(reqBody *models.ScrappingEcommerceParam) error
	GetPopularProduct() (interface{}, error)
	ScrapAllEcommerce(reqBody *models.ScrapAllEcommerceParam) error
	GetProductPeopleChoose(reqBody *models.ProductPeopleChooseParam) (interface{}, error)

	//User
	Register(u *models.User, conn *sql.DB) error
	GetAuthToken(u *models.User) (string, error)
	IsAuthenticated(u *models.User, conn *sql.DB) (string, string, string, string, error)
	GetPersonalInformationUser(userId string, conn *sql.DB) (interface{}, error)
	UpdateUserInformation(userId string, conn *sql.DB) interface{}
	InsertWishListUser(wishlist *models.StorageWishListUser, conn *sql.DB) error
	GetWishListUser(userId string, conn *sql.DB) (interface{}, error)
	DeleteWishListUser(userId, product_id string, conn *sql.DB) (interface{}, error)
	EditProfile(reqParam *models.EditProfileParam, conn *sql.DB) interface{}
}
