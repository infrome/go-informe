package services

import (
	"go-informe/interfaces"
)

// service : Usecase layer (business logic)
type service struct {
	repo interfaces.Repository
}

// NewService : Init new service
func NewService(r interfaces.Repository) interfaces.Service {
	return &service{
		repo: r,
	}
}
