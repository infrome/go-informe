package services

import (
	"database/sql"
	"go-informe/models"
)

func (s *service) Register(u *models.User, conn *sql.DB) error {
	err := s.repo.Register(u, conn)
	if err != nil {
		return err
	}
	return err
}

func (s *service) GetAuthToken(u *models.User) (string, error) {
	data, err := s.repo.GetAuthToken(u)
	if err != nil {
		return data, err
	}
	return data, err
}

func (s *service) IsAuthenticated(u *models.User, conn *sql.DB) (string, string, string, string, error) {
	id, username, email, role, err := s.repo.IsAuthenticated(u, conn)
	if err != nil {
		return id, username, email, role, err
	}
	return id, username, email, role, err
}

func (s *service) GetPersonalInformationUser(userId string, conn *sql.DB) (interface{}, error) {
	data, err := s.repo.GetPersonalInformationUser(userId, conn)
	if err != nil {
		return data, err
	}
	return data, err
}

func (s *service) UpdateUserInformation(userId string, conn *sql.DB) interface{} {
	data := s.repo.UpdateUserInformation(userId, conn)

	return data
}

func (s *service) InsertWishListUser(wishlist *models.StorageWishListUser, conn *sql.DB) error {
	err := s.repo.InsertWishListUser(wishlist, conn)

	return err
}

func (s *service) GetWishListUser(userId string, conn *sql.DB) (interface{}, error) {
	data, err := s.repo.GetWishListUser(userId, conn)
	if err != nil {
		return data, err
	}
	return data, err
}

func (s *service) DeleteWishListUser(userId, product_id string, conn *sql.DB) (interface{}, error) {
	data, err := s.repo.DeleteWishListUser(userId, product_id, conn)
	if err != nil {
		return data, err
	}
	return data, err
}

func (s *service) EditProfile(reqParam *models.EditProfileParam, conn *sql.DB) interface{} {
	err := s.repo.EditProfile(reqParam, conn)
	if err != nil {
		return err
	}
	return err
}
