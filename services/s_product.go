package services

import (
	"database/sql"
	"go-informe/models"
)

func (s *service) GetCategoryProduct() interface{} {
	data := s.repo.GetCategoryProduct()

	return data
}

func (s *service) InsertComment(c *models.Comment, conn *sql.DB) interface{} {
	data := s.repo.InsertComment(c, conn)

	return data
}

func (s *service) InsertReplyComment(c *models.ReplyComment, conn *sql.DB) interface{} {
	data := s.repo.InsertReplyComment(c, conn)

	return data
}

func (s *service) InsertReviewProduct(r *models.Review, conn *sql.DB) interface{} {
	data := s.repo.InsertReviewProduct(r, conn)

	return data
}

func (s *service) GetParentsCommentByProductID(productId, limit, offset string, conn *sql.DB) models.GetAllCommentJSON {
	data := s.repo.GetParentsCommentByProductID(productId, limit, offset, conn)

	return data
}

func (s *service) GetTotalDataComment(productId string, conn *sql.DB) models.GetCountData {
	data := s.repo.GetTotalDataComment(productId, conn)

	return data
}

func (s *service) GetRepliesCommentByProductId(productId string, conn *sql.DB) []models.GetComment {
	data := s.repo.GetRepliesCommentByProductId(productId, conn)

	return data
}

func (s *service) GetAllReviewByPorductID(params models.RequestGetAllReview, conn *sql.DB) interface{} {
	data := s.repo.GetAllReviewByPorductID(params, conn)

	return data
}

func (s *service) GetAllReplyCommentByProductID(productId string, conn *sql.DB) interface{} {
	data := s.repo.GetAllReplyCommentByProductID(productId, conn)

	return data
}
func (s *service) GetAllDataProduct() []models.ScrapNewData {
	data := s.repo.GetAllDataProduct()

	return data
}
func (s *service) SearchNavbarProduct(keyword string) []models.ScrapNewData {
	data := s.repo.SearchNavbarProduct(keyword)

	return data
}
func (s *service) GetProductDetail(keyword string) []models.ScrapNewData {
	data := s.repo.GetProductDetail(keyword)

	return data
}

func (s *service) GetEcommerceData(reqParams *models.ReqDataToEcommerce) []models.GetEcommerceData {
	data := s.repo.GetEcommerceData(reqParams)
	return data
}
func (s *service) GetListDataByCategory(reqParams *models.ReqListDataByCategory) interface{} {
	data := s.repo.GetListDataByCategory(reqParams)
	return data
}
func (s *service) GetRecommendUpgradeProduct(reqParams *models.ReqUpgradeProduct) []models.PhoneData {
	data := s.repo.GetRecommendUpgradeProduct(reqParams)
	return data
}

func (s *service) DeleteComment(reqParams *models.DeleteCommentParam) interface{} {
	data := s.repo.DeleteComment(reqParams)
	return data
}

func (s *service) UpgradeProduct(reqParams *models.UpgradeProductParam) (interface{}, error) {
	data, err := s.repo.UpgradeProduct(reqParams)
	return data, err
}

func (s *service) GetScrappingEcommerce(reqBody *models.ScrappingEcommerceParam) error {
	err := s.repo.GetScrappingEcommerce(reqBody)
	return err
}

func (s *service) GetPopularProduct() (interface{}, error) {
	data, err := s.repo.GetPopularProduct()
	return data, err
}

func (s *service) ScrapAllEcommerce(reqBody *models.ScrapAllEcommerceParam) error {
	err := s.repo.ScrapAllEcommerce(reqBody)
	return err
}

func (s *service) GetProductPeopleChoose(reqParams *models.ProductPeopleChooseParam) (interface{}, error) {
	data, err := s.repo.GetProductPeopleChoose(reqParams)
	return data, err
}
