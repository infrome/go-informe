package controllers

import (
	"encoding/json"
	"go-informe/models"
	"net/http"
	"strconv"

	"github.com/labstack/echo"
)

func (ct *Controller) GetAllCategory(c echo.Context) error {
	response := models.Response{}
	getCategory := ct.Service.GetCategoryProduct()
	response.Data = getCategory
	return c.JSON(http.StatusOK, response)
}

func (ct *Controller) SaveComment(c echo.Context) error {
	response := models.Response{}
	commentCreate := models.Comment{}
	err := c.Bind(&commentCreate)
	if err != nil {
		response.Messages = append(response.Messages, "Failed to bind data")
		return c.JSON(http.StatusBadRequest, response)
	}
	modelDB := new(models.DatabaseSql)
	conn := modelDB.ConnectSql()
	insertComment := ct.Service.InsertComment(&commentCreate, conn)
	response.Data = insertComment
	return c.JSON(http.StatusOK, response)
}

func (ct *Controller) SaveReplyComment(c echo.Context) error {
	response := models.Response{}
	replyComment := models.ReplyComment{}
	err := c.Bind(&replyComment)
	if err != nil {
		response.Messages = append(response.Messages, "Failed for bind data")
		return c.JSON(http.StatusBadRequest, response)
	}
	modelDB := new(models.DatabaseSql)
	conn := modelDB.ConnectSql()
	insertComment := ct.Service.InsertReplyComment(&replyComment, conn)
	response.Data = insertComment
	return c.JSON(http.StatusOK, response)
}

func (ct *Controller) GetCommentProduct(c echo.Context) error {
	response := models.Response{}

	// reqParams := models.ReqParam{}
	productId := c.Param("product_id")
	limit := c.QueryParam("limit")
	offset := c.QueryParam("offset")
	// respGetComment := []models.GetResponseComment{}
	model := new(models.DatabaseSql)
	// getDataComments := models.GetParentsComment{}
	// arrDataComments := []models.GetParentsComment{}
	// childData := models.ChildComment{}
	conn := model.ConnectSql()
	responseData := ct.Service.GetParentsCommentByProductID(productId, limit, offset, conn)
	// commentData := ct.Service.GetRepliesCommentByProductId(parentData.DiscussionId, conn)
	// countData := ct.Service.GetTotalDataComment(productId, conn)
	// for _, val := range parentData {
	// 	getDataComments.UserId = val.UserId
	// 	getDataComments.Comment = val.Comment
	// 	getDataComments.Name = val.Name
	// 	arrDataComments = append(arrDataComments, getDataComments)
	// }
	// for i := 0; i < len(arrDataComments); i++ {
	// 	for _, val := range commentData {
	// 		if arrDataComments[i].UserId == val.ReplyTouserId {
	// 			childData.ReplyToUserId = val.ReplyTouserId
	// 			childData.Name = val.ReplyComment
	// 			childData.Comment = val.ReplyName
	// 			arrDataComments[i].ChildComment = append(arrDataComments[i].ChildComment, childData)
	// 		}
	// 	}
	// }
	// jsonComment := models.CollectJsonComment{}
	// jsonComment.Comment = arrDataComments
	// jsonComment.CountData = countData.CountData
	response.Data = responseData
	return c.JSON(http.StatusOK, response)
}
func (ct *Controller) GetReviewProduct(c echo.Context) error {
	response := models.Response{}
	// reqParams := models.ReqParam{}
	reqParam := models.RequestGetAllReview{}
	reqParam.ProductId = c.Param("product_id")
	reqParam.Limit = c.QueryParam("limit")
	reqParam.Offset = c.QueryParam("offset")
	model := new(models.DatabaseSql)
	conn := model.ConnectSql()
	commentData := ct.Service.GetAllReviewByPorductID(reqParam, conn)
	response.Data = commentData
	return c.JSON(http.StatusOK, response)
}

func (ct *Controller) SaveReviewProduct(c echo.Context) error {
	productId := c.Param("product_id")
	response := models.Response{}
	reviewData := models.Review{}
	err := c.Bind(&reviewData)
	if productId != "" {
		convProductId, _ := strconv.Atoi(productId)
		reviewData.Productid = convProductId
	}
	if err != nil {
		response.Messages = append(response.Messages, "Failed for bind data")
		return c.JSON(http.StatusBadRequest, response)
	}
	modelDB := new(models.DatabaseSql)
	conn := modelDB.ConnectSql()
	insertComment := ct.Service.InsertReviewProduct(&reviewData, conn)
	response.Data = insertComment
	return c.JSON(http.StatusOK, response)
}

func (ct *Controller) GetAllProductFromMongo(c echo.Context) error {
	response := models.Response{}

	data := ct.Service.GetAllDataProduct()
	// if err != nil {
	// 	return c.JSON(http.StatusBadRequest, response)
	// }

	a, _ := json.Marshal(data)
	respData := []models.RespProductMongo{}
	json.Unmarshal(a, &respData)
	response.Data = data
	return c.JSON(http.StatusOK, response)
}

func (ct *Controller) GetSearchNavbar(c echo.Context) error {
	response := models.Response{}
	keyword := c.Param("keyword")
	data := ct.Service.SearchNavbarProduct(keyword)
	response.Data = data
	return c.JSON(http.StatusOK, response)
}
func (ct *Controller) GetProductDetail(c echo.Context) error {
	response := models.Response{}
	keyword := c.Param("product_id")
	data := ct.Service.GetProductDetail(keyword)
	response.Data = data
	return c.JSON(http.StatusOK, response)
}
func (ct *Controller) GetPriceEcommerce(c echo.Context) error {
	response := models.Response{}
	productId := c.QueryParam("product_id")
	// nameProduct := c.QueryParam("name_product")
	reqParams := models.ReqDataToEcommerce{}
	// reqParams.NameProduct = nameProduct
	reqParams.ProductID = productId
	data := ct.Service.GetEcommerceData(&reqParams)
	// if err != nil {
	// 	return c.JSON(http.StatusBadRequest, response)
	// }

	a, _ := json.Marshal(data)
	respData := []models.RespProductMongo{}
	json.Unmarshal(a, &respData)
	response.Data = data
	return c.JSON(http.StatusOK, response)
}
func (ct *Controller) GetListProductByCategory(c echo.Context) error {
	response := models.Response{}
	categoryProduct := c.QueryParam("category_product")
	reqParams := models.ReqListDataByCategory{}
	reqParams.CategoryProduct = categoryProduct

	data := ct.Service.GetListDataByCategory(&reqParams)

	a, _ := json.Marshal(data)
	respData := []models.RespProductMongo{}
	json.Unmarshal(a, &respData)
	response.Data = data
	return c.JSON(http.StatusOK, response)
}

func (ct *Controller) GetUpgradeProduct(c echo.Context) error {
	response := models.Response{}
	reqParams := models.ReqUpgradeProduct{}
	err := c.Bind(&reqParams)
	if err != nil {
	}
	data := ct.Service.GetRecommendUpgradeProduct(&reqParams)

	a, _ := json.Marshal(data)
	respData := []models.RespProductMongo{}
	json.Unmarshal(a, &respData)
	response.Data = data
	return c.JSON(http.StatusOK, response)
}

func (ct *Controller) DeleteComment(c echo.Context) error {
	response := models.Response{}
	reqParams := models.DeleteCommentParam{}
	err := c.Bind(&reqParams)
	if err != nil {

		return c.JSON(http.StatusInternalServerError, response)
	}
	if reqParams.DiscussionID <= 0 {
		errorMessage := "Discussion ID can't be empty"
		response.Messages = append(response.Messages, errorMessage)
		return c.JSON(http.StatusBadRequest, response)
	}

	eror := ct.Service.DeleteComment(&reqParams)
	if eror != nil {
		response.Messages = append(response.Messages, "Failed to Delete Comment")
		return c.JSON(http.StatusAccepted, response)
	}
	response.Messages = append(response.Messages, "Success")
	return c.JSON(http.StatusOK, response)
}

func (ct *Controller) UpgradeProduct(c echo.Context) error {
	response := models.Response{}
	productId := c.QueryParam("product_id")
	budget := c.QueryParam("budget")
	budgetInt, _ := strconv.Atoi(budget)
	reqParams := models.UpgradeProductParam{}

	reqParams.Budget = budgetInt
	reqParams.ProductID = productId

	// if reqParams.DiscussionID <= 0 {
	// 	errorMessage := "Discussion ID can't be empty"
	// 	response.Messages = append(response.Messages, errorMessage)
	// 	return c.JSON(http.StatusBadRequest, response)
	// }

	data, eror := ct.Service.UpgradeProduct(&reqParams)
	if eror != nil {
		response.Messages = append(response.Messages, "UPGRADE DATA ERROR : "+eror.Error())
		return c.JSON(http.StatusAccepted, response)
	}
	response.Data = data
	response.Messages = append(response.Messages, "Success")
	return c.JSON(http.StatusOK, response)
}

func (ct *Controller) ScrappingEcommerce(c echo.Context) error {
	response := models.Response{}
	reqBody := models.ScrappingEcommerceParam{}
	err := c.Bind(&reqBody)
	if err != nil {

		return c.JSON(http.StatusInternalServerError, response)
	}

	if len(reqBody.Web) <= 0 {
		errorMessage := "Web not found"
		response.Messages = append(response.Messages, errorMessage)
		return c.JSON(http.StatusBadRequest, response)
	}

	error := ct.Service.GetScrappingEcommerce(&reqBody)
	if error != nil {
		response.Messages = append(response.Messages, "Failed to Scrap Ecommerce")
		return c.JSON(http.StatusAccepted, response)
	}
	response.Messages = append(response.Messages, "Scrap success")
	return c.JSON(http.StatusOK, response)
}

func (ct *Controller) GetPopularProduct(c echo.Context) error {
	response := models.Response{}

	data, err := ct.Service.GetPopularProduct()
	if err != nil {
		return c.JSON(http.StatusBadRequest, response)
	}

	a, _ := json.Marshal(data)
	respData := []models.RespProductMongo{}
	json.Unmarshal(a, &respData)
	response.Data = data
	return c.JSON(http.StatusOK, response)
}

func (ct *Controller) ScrapAllEcommerce(c echo.Context) error {
	response := models.Response{}
	reqBody := models.ScrapAllEcommerceParam{}
	err := c.Bind(&reqBody)
	if err != nil {

		return c.JSON(http.StatusInternalServerError, response)
	}

	// if len(reqBody.Web) <= 0 {
	// 	errorMessage := "Web not found"
	// 	response.Messages = append(response.Messages, errorMessage)
	// 	return c.JSON(http.StatusBadRequest, response)
	// }

	error := ct.Service.ScrapAllEcommerce(&reqBody)
	if error != nil {
		response.Messages = append(response.Messages, "Failed to Scrap Ecommerce")
		return c.JSON(http.StatusAccepted, response)
	}
	response.Messages = append(response.Messages, "Scrap success")
	return c.JSON(http.StatusOK, response)
}

func (ct *Controller) GetProductPeopleChoose(c echo.Context) error {
	response := models.Response{}
	productId := c.QueryParam("product_id")

	reqParams := models.ProductPeopleChooseParam{}
	reqParams.ProductID = productId

	data, err := ct.Service.GetProductPeopleChoose(&reqParams)
	if err != nil {
		return c.JSON(http.StatusBadRequest, response)
	}

	a, _ := json.Marshal(data)
	respData := []models.RespProductMongo{}
	json.Unmarshal(a, &respData)
	response.Data = data
	return c.JSON(http.StatusOK, response)
}
