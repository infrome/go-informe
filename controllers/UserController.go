package controllers

import (
	"fmt"
	"go-informe/models"
	"net/http"

	"github.com/labstack/echo"
)

func (ct *Controller) UserRegister(c echo.Context) error {
	user := models.User{}
	response := models.Response{}
	err := c.Bind(&user)
	if err != nil {
		// return c.JSON(http.StatusBadRequest, err.Error())
	}
	model := new(models.DatabaseSql)
	conn := model.ConnectSql()
	err = ct.Service.Register(&user, conn)
	conn.Close()
	if err != nil {
		if err != nil {
			fmt.Println("Error in user.Register()")

			return c.JSON(http.StatusBadRequest, err.Error())
		}
	}
	return c.JSON(http.StatusOK, response)
}

func (ct *Controller) UserLogin(c echo.Context) error {
	user := models.User{}
	response := models.Response{}
	dataUser := models.StorageInformationUser{}
	err := c.Bind(&user)
	if err != nil {

	}
	model := new(models.DatabaseSql)
	conn := model.ConnectSql()
	userId, username, email, role, err := ct.Service.IsAuthenticated(&user, conn)
	dataUser.Id = userId
	dataUser.Email = email
	dataUser.UserName = username
	dataUser.Role = role
	conn.Close()
	if err != nil {
		var arr []string
		response.Data = arr
		response.Messages = append(response.Messages, "Login Failed")
		return c.JSON(http.StatusOK, response)
	}
	token, err := ct.Service.GetAuthToken(&user)
	dataUser.Token = token
	// response.Data =
	response.Data = dataUser
	return c.JSON(http.StatusOK, response)
}

func (ct *Controller) GetUserProfile(c echo.Context) error {
	response := models.Response{}
	userId := c.Param("user_id")
	model := new(models.DatabaseSql)
	conn := model.ConnectSql()
	data, err := ct.Service.GetPersonalInformationUser(userId, conn)
	conn.Close()
	if err != nil {
		return c.JSON(http.StatusBadRequest, "")
	}
	response.Data = data
	return c.JSON(http.StatusOK, response)
}
func (ct *Controller) UpdateUserProfile(c echo.Context) error {
	response := models.Response{}
	userId := c.Param("user_id")
	model := new(models.DatabaseSql)
	conn := model.ConnectSql()
	err := ct.Service.UpdateUserInformation(userId, conn)
	conn.Close()
	if err != nil {
		return c.JSON(http.StatusBadRequest, "")
	}
	return c.JSON(http.StatusOK, response)
}
func (ct *Controller) InsertIntoWishList(c echo.Context) error {
	dataWishlist := models.StorageWishListUser{}
	response := models.Response{}
	err := c.Bind(&dataWishlist)
	if err != nil {
		response.Errors = "Failed for bind data"
		return c.JSON(http.StatusBadRequest, response)
	}
	model := new(models.DatabaseSql)
	conn := model.ConnectSql()
	err = ct.Service.InsertWishListUser(&dataWishlist, conn)
	conn.Close()
	if err != nil {
		response.Errors = "Failed for insert data product to wishlist"
		return c.JSON(http.StatusBadRequest, response)
	}
	response.Data = "sukses insert into wishlist"
	return c.JSON(http.StatusOK, response)
}

func (ct *Controller) GetWishListUser(c echo.Context) error {
	response := models.Response{}
	userId := c.Param("user_id")
	model := new(models.DatabaseSql)
	conn := model.ConnectSql()
	data, err := ct.Service.GetWishListUser(userId, conn)
	conn.Close()
	if err != nil {
		return c.JSON(http.StatusBadRequest, response)
	}
	response.Data = data
	return c.JSON(http.StatusOK, response)
}
func (ct *Controller) DeleteWishListUser(c echo.Context) error {
	response := models.Response{}
	reqParams := models.ReqDeleteWishListUser{}
	err := c.Bind(&reqParams)
	model := new(models.DatabaseSql)
	conn := model.ConnectSql()
	data, err := ct.Service.DeleteWishListUser(reqParams.Id, reqParams.ProductId, conn)
	conn.Close()
	if err != nil {
		return c.JSON(http.StatusBadRequest, response)
	}
	response.Data = data
	return c.JSON(http.StatusOK, response)
}

func (ct *Controller) EditProfile(c echo.Context) error {
	response := models.Response{}
	reqParams := models.EditProfileParam{}
	err := c.Bind(&reqParams)
	if reqParams.UserId == "" {
		response.Messages = append(response.Messages, "USER ID MUST BE FILLED")
		return c.JSON(http.StatusBadRequest, response)
	}
	model := new(models.DatabaseSql)
	conn := model.ConnectSql()
	data := ct.Service.EditProfile(&reqParams, conn)
	conn.Close()
	if data != nil {
		response.Errors = err
		return c.JSON(http.StatusBadRequest, response)
	}
	response.Messages = append(response.Messages, "SUCCESS UPDATE PROFILE")
	return c.JSON(http.StatusOK, response)
}
