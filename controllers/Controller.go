package controllers

import (
	"go-informe/interfaces"

	"github.com/labstack/echo"
)

// Controller : Delivery layer (REST)
type Controller struct {
	Service interfaces.Service
}

// NewController : Init controller
func NewController(e *echo.Echo, s interfaces.Service) {
	controller := &Controller{
		Service: s,
	}

	UserRoute(e, controller)
	ProducRoute(e, controller)
	ScrapRoute(e, controller)

}

func UserRoute(e *echo.Echo, controllers *Controller) {
	UserPrefix := e.Group("/user")
	UserPrefix.POST("/register", controllers.UserRegister)
	UserPrefix.POST("/login", controllers.UserLogin)
	UserPrefix.GET("/profile/:user_id", controllers.GetUserProfile)
	UserPrefix.PUT("/profile/:user_id", controllers.UpdateUserProfile)
	UserPrefix.POST("/wishlist", controllers.InsertIntoWishList)
	UserPrefix.GET("/get_wishlist/:user_id", controllers.GetWishListUser)
	UserPrefix.POST("/delete_wishlist", controllers.DeleteWishListUser)
	UserPrefix.POST("/profile/edit", controllers.EditProfile)
}
func ProducRoute(e *echo.Echo, controllers *Controller) {
	ProductPrefix := e.Group("/product")
	ProductPrefix.GET("/listcategory", controllers.GetAllCategory)
	ProductPrefix.GET("/getListDataByCategory", controllers.GetListProductByCategory)
	ProductPrefix.GET("/getAllComment/:product_id", controllers.GetCommentProduct)
	ProductPrefix.GET("/getAllReview/:product_id", controllers.GetReviewProduct)
	ProductPrefix.POST("/insert_comment", controllers.SaveComment)
	ProductPrefix.POST("/insert_reply_comment", controllers.SaveReplyComment)
	ProductPrefix.POST("/insert_review/:product_id", controllers.SaveReviewProduct)
	ProductPrefix.GET("/getallproduct", controllers.GetAllProductFromMongo)
	ProductPrefix.GET("/getPriceEcommerce", controllers.GetPriceEcommerce)
	ProductPrefix.GET("/searchNavbar/:keyword", controllers.GetSearchNavbar)
	ProductPrefix.GET("/getproductdetail/:product_id", controllers.GetProductDetail)
	ProductPrefix.GET("/upgradeProduct", controllers.GetUpgradeProduct)
	ProductPrefix.POST("/comment/delete", controllers.DeleteComment)
	ProductPrefix.GET("/upgrade", controllers.UpgradeProduct)
	ProductPrefix.POST("/scrapEcommerce", controllers.ScrappingEcommerce)
	ProductPrefix.GET("/getPopularProduct", controllers.GetPopularProduct)
	ProductPrefix.POST("/scrap/all/ecommerce", controllers.ScrapAllEcommerce)
	ProductPrefix.GET("/getProductPeopleChoose", controllers.GetProductPeopleChoose)
}

func AdminRoute(e *echo.Echo, controller *Controller) {
	// AdminPrefix := e.Group("/action")
	// AdminPrefix.DELETE("/delete_comment")
}
func ScrapRoute(e *echo.Echo, controllers *Controller) {
	//ScrapPrefix := e.Group("/scrap")
	// ScrapPrefix.Get("/", helpers.GetAllDataFromMongoDB)

}
