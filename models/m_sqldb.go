package models

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

type DatabaseSql struct {
	Connection *sql.DB
}

// func ConnectMongoDB() (*mgo.Session, error) {
// 	mongoDBDialInfo := &mgo.DialInfo{
// 		Addrs:    []string{os.Getenv("MONGO_HOST")},
// 		Timeout:  120 * time.Second, // 120s of timeout
// 		Database: os.Getenv("MONGO_DB"),
// 	}
// 	session, err := mgo.DialWithInfo(mongoDBDialInfo)
// 	if err != nil {
// 		return nil, err
// 	}

// 	return session, nil
// }
func (db *DatabaseSql) ConnectSql() *sql.DB {
	// get environment database sql
	// Username := os.Getenv("DB_USER_MASTER")
	// Password := os.Getenv("DB_PASS_MASTER")
	// Host := os.Getenv("DB_HOST_MASTER")
	// Port := os.Getenv("DB_PORT_MASTER")
	// DatabaseName := os.Getenv("DB_HOST_MASTER")
	if db.Connection == nil {
		ConnectionString := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", "admin", "asdfg123", "brilliant.coie8l2qbkic.ap-southeast-1.rds.amazonaws.com", "3306", "Informe")
		conn, err := sql.Open("mysql", ConnectionString)
		// err = db.Connection.Ping()
		db.Connection = conn
		if err != nil || conn == nil {
			fmt.Println("Error connecting to DB")
			fmt.Println(err.Error())
		}
	}

	return db.Connection
}
