package models

type StorageData struct {
	ID            string `bson:"_id"`
	Title         string `bson:"title"`
	Spesification string `bson:"spesification"`
}
type SpesificationProductGsmArena struct {
	Title         string `selector:"#specs-phone-name-title", bson:"title"`
	Spesification string `selector:"#specs-list", bson:"specs"`
	Photo         string `selector:"#specs-photo-main",bson:"url_photo"`
	UrlPhoto      string
	ProductId     string
	Brands        string
	Category      string
}
type PhoneData struct {
	ProductId    string             `bson:"product_id"`
	Title        string             `bson:"title"`
	UrlPhoto     string             `bson:"url_photo"`
	LowestPrice  int                `bson:"lowest_price"`
	Category     string             `bson:"category_product"`
	Network      DetailNetwork      `bson:"network"`
	Launch       DetailLaunch       `bson:"launch"`
	Body         DetailBody         `bson:"body"`
	Display      DetailDisplay      `bson:"display"`
	Platform     DetailPlatform     `bson:"platform"`
	Memory       DetailMemory       `bson:"memory"`
	MainCamera   DetailMainCamera   `bson:"maincamera"`
	SelfieCamera DetailSelfieCamera `bson:"selfiecamera"`
	Sound        DetailSound        `bson:"sound"`
	Comms        DetailComms        `bson:"comms"`
	Features     DetailFeatures     `bson:"features"`
	Battery      DetailBattery      `bson:"battery"`
	Misc         DetailMisc         `bson:"misc"`
}
type DetailNetwork struct {
	Technology  string `bson:"technology"`
	TwoGBands   string `bson:"2g_bands"`
	ThreeGBands string `bson:"3g_bands"`
	FourGBands  string `bson:"4g_bands"`
	Speed       string `bson:"speed"`
}
type DetailLaunch struct {
	Announced string `bson:"announced"`
	Status    string `bson:"status"`
}
type DetailBody struct {
	Dimensions string `bson:"dimensions"`
	Weight     string `bson:"weight"`
	Build      string `bson:"build"`
	Sim        string `bson:"sim"`
}
type DetailDisplay struct {
	Type       string `bson:"type"`
	Size       string `bson:"size"`
	Resolution string `bson:"resolution"`
}
type DetailPlatform struct {
	Os      string `bson:"os"`
	Chipset string `bson:"chipset"`
	Cpu     string `bson:"cpu"`
	Gpu     string `bson:"gpu"`
}
type DetailMemory struct {
	CardSlot string `bson:"card_slot"`
	Internal string `bson:"internal"`
}
type DetailMainCamera struct {
	Triple   string `bson:"triple"`
	Quad     string `bson:"quad"`
	Features string `bson:"features"`
	Video    string `bson:"video"`
}
type DetailSelfieCamera struct {
	Single   string `bson:"single"`
	Features string `bson:"features"`
	Video    string `bson:"video"`
}
type DetailSound struct {
	Loudspeaker string `bson:"loudspeaker"`
	IsJack      string `bson:"3.5mm_jack"`
}
type DetailComms struct {
	Wlan      string `bson:"wlan"`
	Bluetooth string `bson:"bluetooth"`
	Gps       string `bson:"gps"`
	Nfc       string `bson:"nfc"`
	Radio     string `bson:"radio"`
	Usb       string `bson:"usb"`
}
type DetailFeatures struct {
	Sensors string `bson:"sensors"`
}
type DetailBattery struct {
	Type     string `bson:"type"`
	Charging string `bson:"charging"`
}
type DetailMisc struct {
	Colors string `bson:"colors"`
	Models string `bson:"models"`
	SarEU  string `bson:"sar_eu"`
	Price  string `bson:"price"`
}

type GetEcommerceData struct {
	ProductId  string `bson:"product_id"`
	Name       string `bson:"name"`
	Price      string `bson:"price"`
	Rating     string `bson:"rating"`
	SoldQty    int    `bson:"sold"`
	Store      string `bson:"store"`
	UrlProduct string `bson:"url_product"`
	Web        string `bson:"web"`
}

type LaptopData struct {
	ProductId        string           `bson:"product_id"`
	Title            string           `bson:"title"`
	UrlPhoto         string           `bson:"url_photo"`
	LowestPrice      int              `bson:"lowest_price"`
	Tipe             TipeLaptop       `bson:"tipe"`
	SpesifikasiDasar SpesifikasiDasar `bson:"spesifikasi_dasar"`
	MemoryAndStorage MemoryAndStorage `bson:"memoridanpenyimpanan"`
	Layar            Layar            `bson:"layar"`
	Network          Network          `bson:"network"`
	Konektifitas     Konektifitas     `bson:"konektifitas"`
	Software         Software         `bson:"software"`
	Baterai          Baterai          `bson:"baterai"`
	Ukuran           Ukuran           `bson:"ukuran"`
}
type TipeLaptop struct {
	TipeLaptop string `bson:"tipe_laptop"`
}
type SpesifikasiDasar struct {
	CPU           string `bson:"cpu"`
	ModelProsesor string `bson:"model_prosesor"`
	Chipset       string `bson:"chipset"`
	ModelGPU      string `bson:"model_gpu"`
}
type MemoryAndStorage struct {
	Ram             string `bson:"ram"`
	TipeMemori      string `bson:"tipe_memori"`
	KecepatanMemori string `bson:"kecepatan_memori"`
	TipePenyimpanan string `bson:"tipe_penyimpanan"`
	Hdd             string `bson:"hdd"`
	KecepatanRotasi string `bson:"kecepatan_rotasi"`
	SsdEmmc         string `bson:"ssdemmc"`
}
type Layar struct {
	UkuranLayar string `bson:"ukuran_layar"`
	Resolusi    string `bson:"resolusi"`
	TipePanel   string `bson:"tipe_panel"`
}
type Network struct {
	Ethernet string `bson:"ethernet"`
	Wifi     string `bson:"wifi"`
}
type Konektifitas struct {
	Konektifitas string `bson:"konektifitas"`
}
type Software struct {
	Os        string `bson:"os"`
	OsVersion string `bson:"os_ver"`
}
type Baterai struct {
	Baterai string `bson:"baterai"`
}
type Ukuran struct {
	Dimensi string `bson:"dimensi"`
	Berat   string `bson:"berat"`
}

type PhoneScrap struct {
	Category  string `bson:"category"`
	ProductID string `bson:"product_id"`
	Title     string `bson:"title"`
}

type ScrapNewData struct {
	ProductID     string `bson:"product_id"`
	Title         string `bson:"title"`
	Spesification string `bson:"spesification"`
	Photo         string `bson:"photo"`
	LowestPrice   int    `bson:"lowest_price"`
	Category      string `bson:"category"`
}
