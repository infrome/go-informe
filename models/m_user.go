package models

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	"golang.org/x/crypto/bcrypt"
)

type User struct {
	ID              string    `json:"id"`
	UserName        string    `json:"user_name"`
	CreatedAt       time.Time `json:"created_at"`
	UpdatedAt       time.Time `json:"updated_at"`
	Email           string    `json:"email"`
	PasswordHash    string    `json:"password_hash"`
	Password        string    `json:"password"`
	PasswordConfirm string    `json:"password_confirm"`
	Role            string    `json:"role"`
}
type UserInfromation struct {
	ID           string `json:"id"`
	Email        string `json:"email"`
	PasswordHash string `json:"password_hash"`
	UserName     string `json:"user_name"`
	Role         string `json:"role"`
}

// func (u *User) Login(conn *sql.DB) error {

// }

type StorageInformationUser struct {
	Id       string `json:"id"`
	Email    string `json:"email"`
	Token    string `json:"token"`
	UserName string `json:"user_name"`
	Role     string `json:"role"`
}

type StorageWishListUser struct {
	Id        string `json:"id"`
	ProductId string `json:"product_id"`
	WishId    string `json:"wish_id"`
}

type ReqDeleteWishListUser struct {
	Id        string `json:"id"`
	ProductId string `json:"product_id"`
}

type EditProfileParam struct {
	UserId   string `json:"user_id"`
	UserName string `json:"user_name"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

func Register(u *User, conn *sql.DB) error {
	if len(u.Password) < 4 || len(u.PasswordConfirm) < 4 {
		return fmt.Errorf("Password must be at least 4 characters long.")
	}
	if len(u.UserName) < 0 {
		return fmt.Errorf("UserName Must Be Filled")
	}
	if u.Password != u.PasswordConfirm {
		return fmt.Errorf("Passwords do not match.")
	}

	if len(u.Email) < 4 {
		return fmt.Errorf("Email must be at least 4 characters long.")
	}

	u.Email = strings.ToLower(u.Email)
	row := conn.QueryRow("SELECT user_name,email FROM user_account where email = ?  AND user_name = ?", u.Email, u.UserName)
	checkExistingData := User{}
	_ = row.Scan(&checkExistingData.Email, &checkExistingData.UserName)

	if checkExistingData.Email == u.Email {
		return fmt.Errorf("Email already exists ! please choose another email")
	}
	if checkExistingData.UserName == u.UserName {
		return fmt.Errorf("UserName already exists ! please choose another UserName")
	}
	pwdHash, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	if err != nil {
		return fmt.Errorf("There was an error creating your account.")
	}
	u.PasswordHash = string(pwdHash)

	_, err = conn.Exec("INSERT INTO user_account (created_at,user_name, email, password_hash) VALUES(?,?, ?, ?)", time.Now(), u.UserName, u.Email, u.PasswordHash)

	return err

}
