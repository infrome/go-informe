package models

type Category struct {
	Category string `json:"name_category",bson:"categoryu`
}

type Comment struct {
	Userid    int    `json:"user_id"`
	Productid int    `json:"product_id"`
	Comment   string `json:"comment"`
}

type ReplyComment struct {
	UserId       int    `json:"user_id"`
	DiscussionId int    `json:"discussion_id"`
	ReplyComment string `json:"reply_comment"`
}

type GetComment struct {
	UserId        int    `json:"user_id"`
	Name          string `json:"name"`
	ReplyTouserId int    `json:"replyto_user_id"`
	Comment       string `json:"comment"`
	ReplyName     string `json:"reply_name"`
	ReplyComment  string `json:"reply_comment"`
}

type ReqParam struct {
	ProductId string `json:"product_id"`
}
type Review struct {
	Productid     int    `json:"product_id"`
	ReviewName    string `json:"review_name"`
	ReviewComment string `json:"review_comment"`
	Star          int    `json:"star"`
}

type GetReview struct {
	ReviewName    string `json:"review_name"`
	ReviewComment string `json:"review_comment"`
	Star          int    `json:"star"`
}
type CalculateAverageStar struct {
	AverageStar float64 `json:"avg_star"`
}
type GetCountData struct {
	CountData int `json:"count_data"`
}
type RequestGetAllReview struct {
	ProductId string `json:"product_id"`
	Offset    string `json:"offset"`
	Limit     string `json:"limit"`
}
type GetParentsComment struct {
	UserId       int            `json:"user_id"`
	Name         string         `json:"name"`
	Comment      string         `json:"comment"`
	ChildComment []ChildComment `json:"replies"`
}
type ChildComment struct {
	ReplyToUserId int    `json:"replyto_user_id"`
	Name          string `json:"name"`
	Comment       string `json:"comment"`
}
type GetParentJson struct {
	DiscussionID int    `json:"discussion_id"`
	UserId       int    `json:"user_id"`
	Name         string `json:"name"`
	Comment      string `json:"comment"`
}

type CollectJsonComment struct {
	Comment   interface{} `json:"comment"`
	CountData int         `json:"count_data"`
}

type RespProductMongo struct {
	ProductId string `json:"ProductId",bson:"ProductId"`
	Title     string `json:"Title",bson:"Title"`
	UrlPhoto  string `json:"UrlPhoto", bson:"UrlPhoto"`
}

type ReqDataToEcommerce struct {
	ProductID string `json:"product_id",bson:"product_id"`
	// NameProduct string `json:"name_product",bson:"name_product"`
}
type ReqListDataByCategory struct {
	CategoryProduct string `json:"category_product", bson:"category_product"`
}
type RespListCategory struct {
	CategoryProduct string `bson:"category_product"`
}
type ReqUpgradeProduct struct {
	ProductId string `json:"product_id",bson:"product_id"`
	Budget    string `json:"budget_price",bson:"budget_price"`
}

type ResultReplyComment struct {
	ReplyID      int    `json:"reply_discussion_id"`
	UserID       int    `json:"user_id"`
	ReplyComment string `json:"reply_comment"`
}

type ResultReplyCommentJSON struct {
	ReplyID      int    `json:"reply_discussion_id"`
	Name         string `json:"name"`
	ReplyComment string `json:"reply_comment"`
}

type ResultGetUsername struct {
	Name string `json:"name"`
}

type GetAllCommentJSON struct {
	Comment interface{} `json:"comment"`
	Total   int         `json:"total"`
}

type CommentJSON struct {
	DiscussionID int         `json:"discussion_id"`
	Name         string      `json:"name"`
	Comment      string      `json:"comment"`
	Replies      interface{} `json:"replies"`
}

type DeleteCommentParam struct {
	DiscussionID      int `json:"discussion_id"`
	ReplyDiscussionID int `json:"reply_discussion_id"`
}

type UpgradeProductParam struct {
	ProductID string `json:"product_id"`
	Budget    int    `json:"budget"`
}

type ScrappingEcommerceParam struct {
	Url         string `json:"url"`
	ProductName string `json:"product_name"`
	Web         string `json:"web"`
	ProductID   string `json:"product_id"`
}

type ScrappingProductEcommerce struct {
	ProductId  string `bson:"product_id"`
	Name       string `bson:"name"`
	Price      string `bson:"price"`
	Rating     string `bson:"rating"`
	Sold       int    `bson:"sold"`
	Store      string `bson:"store"`
	UrlProduct string `bson:"url_product"`
	Web        string `bson:"web"`
}

type FindProductEcommerce struct {
	ProductId  string `bson:"product_id"`
	Name       string `bson:"name"`
	Price      string `bson:"price"`
	Rating     string `bson:"rating"`
	Sold       int    `bson:"sold"`
	Store      string `bson:"store"`
	UrlProduct string `bson:"url_product"`
	Web        string `bson:"web"`
}

type TempPhoneData struct {
	// ProductId    string             `bson:"product_id"`
	// Title        string             `bson:"title"`
	// UrlPhoto     string             `bson:"url_photo"`
	// LowestPrice  int                `bson:"lowest_price"`
	// Category     string             `bson:"category_product"`
	// Network      DetailNetwork      `bson:"network"`
	// Launch       DetailLaunch       `bson:"launch"`
	// Body         DetailBody         `bson:"body"`
	// Display      DetailDisplay      `bson:"display"`
	// Platform     DetailPlatform     `bson:"platform"`
	// Memory       DetailMemory       `bson:"memory"`
	// MainCamera   DetailMainCamera   `bson:"maincamera"`
	// SelfieCamera DetailSelfieCamera `bson:"selfiecamera"`
	// Sound        DetailSound        `bson:"sound"`
	// Comms        DetailComms        `bson:"comms"`
	// Features     DetailFeatures     `bson:"features"`
	// Battery      DetailBattery      `bson:"battery"`
	// Misc         DetailMisc         `bson:"misc"`
	ProductID     string `bson:"product_id"`
	Title         string `bson:"title"`
	Spesification string `bson:"spesification"`
	Photo         string `bson:"photo"`
	LowestPrice   int    `bson:"lowest_price"`
}

type TempLaptopData struct {
	// ProductId        string           `bson:"product_id"`
	// Title            string           `bson:"title"`
	// UrlPhoto         string           `bson:"url_photo"`
	// LowestPrice      int              `bson:"lowest_price"`
	// Tipe             TipeLaptop       `bson:"tipe"`
	// SpesifikasiDasar SpesifikasiDasar `bson:"spesifikasi_dasar"`
	// MemoryAndStorage MemoryAndStorage `bson:"memoridanpenyimpanan"`
	// Layar            Layar            `bson:"layar"`
	// Network          Network          `bson:"network"`
	// Konektifitas     Konektifitas     `bson:"konektifitas"`
	// Software         Software         `bson:"software"`
	// Baterai          Baterai          `bson:"baterai"`
	// Ukuran           Ukuran           `bson:"ukuran"`
	ProductID     string `bson:"product_id"`
	Title         string `bson:"title"`
	Spesification string `bson:"spesification"`
	Photo         string `bson:"photo"`
	LowestPrice   int    `bson:"lowest_price"`
}

type BlibliScrap struct {
	Data BlibliData `json:"data"`
}

type BlibliData struct {
	Products []BlibliProducts `json:"products"`
}

type BlibliProducts struct {
	Name   string       `json:"name"`
	Price  PriceBlibli  `json:"price"`
	Review ReviewBlibli `json:"review"`
	URL    string       `json:"url"`
}

type PriceBlibli struct {
	Price    string  `json:"priceDisplay"`
	IntPrice float64 `json:"minPrice"`
}

type ReviewBlibli struct {
	Sold   int `json:"count"`
	Rating int `json:"rating"`
}

type ScrapAllEcommerceParam struct {
	// Web      string `json:"web"`
	Category string `json:"category_product"`
}

type ProductPeopleChooseParam struct {
	ProductID string `json:"product_id"`
}
