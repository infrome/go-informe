package models

type Response struct {
	Errors   interface{} `json:"errors,omitempty"`
	Data     interface{} `json:"data"`
	Messages []string    `json:"messages,omitempty"`
}
type Errors struct {
	Code     int      `json:"code,omitempty"`
	Title    string   `json:"title,omitempty"`
	Messages []string `json:"messages,omitempty"`
}
