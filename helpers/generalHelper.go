package helpers

import (
	"fmt"
	"go-informe/models"
	"log"
	"net/http/httputil"
	"os"

	"github.com/labstack/echo"
)

func WriteToLogfile(filename string, title string, message string) {
	//create your file with desired read/write permissions
	f, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	//set output of logs to f
	log.SetOutput(f)

	log.Println(message)
}
func ErrorMessage(response models.Response, titleResponse, message, titleLog, messageLog, logFileName string, errorCode int) models.Response {
	response.Errors = models.Errors{
		Code:     errorCode,
		Title:    titleResponse,
		Messages: []string{message},
	}

	response.Messages = append(response.Messages, "error")

	WriteToLogfile("error.log", messageLog, logFileName)

	return response
}

func LogRequestParam() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			req := c.Request()

			// Save a copy of this request for debugging.
			requestDump, err := httputil.DumpRequest(req, true)
			if err != nil {
				fmt.Println(err)
			}

			f, err := os.OpenFile("logs/requestParams.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
			if err != nil {
				panic("error opening file")
			}
			defer f.Close()

			log.SetOutput(f)
			log.Println(":\n" + string(requestDump))

			return next(c)
		}
	}
}
