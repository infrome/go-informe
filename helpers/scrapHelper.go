package helpers

import (
	"strings"
)

func GetIndexTwoString(input string, findstringone, findstringtwo string) (int, int) {
	return strings.Index(input, findstringone), strings.Index(input, findstringtwo)
}
