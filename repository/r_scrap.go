package repository

import (
	"context"
	"encoding/json"
	"fmt"
	"go-informe/models"
	"strings"

	"github.com/gocolly/colly"
	"github.com/gocolly/colly/debug"
)

func ScrapGsmArena(url string) {
	c := colly.NewCollector(colly.Debugger(&debug.LogDebugger{}), colly.UserAgent("xy"))
	specPhone := []models.SpesificationProductGsmArena{}
	temp := models.SpesificationProductGsmArena{}
	c.OnHTML(".review-header", func(e *colly.HTMLElement) {
		temp.Title = e.ChildText(".specs-phone-name-title")

	})
	c.OnHTML(".specs-photo-main > a > img", func(e *colly.HTMLElement) {
		temp.Photo = e.Attr("src")
	})

	c.OnHTML("#specs-list", func(e *colly.HTMLElement) {
		// helpers.WriteToLogfile("resp.log", e.Text)
		temp.Spesification = e.Text
		specPhone = append(specPhone, temp)
		jsonSpec, _ := json.Marshal(specPhone)
		fmt.Println(string(jsonSpec))
		connect := new(models.DatabaseMongo)
		ctx := context.Background()
		session, err := connect.ConnectMongoDB()
		if err != nil {
			fmt.Println("Cannot connect to MongoDB")
		}
		defer session.Disconnect(ctx)
		var collection = session.Database("product").Collection("spesificationProduct")
		collection.InsertOne(ctx, temp)
	})
	c.OnHTML("a[href]", func(h *colly.HTMLElement) {
		if h.Attr("class") == "makers" {
			return
		}
		link := h.Attr("href")
		fmt.Println(link)
		if !strings.HasSuffix(link, ".php") {
			return
		}
		h.Request.Visit(link)
	})
	c.Visit(url)
}

// func ConvertScrapToJsonPhone() {
// 	data := models.PhoneData{}
// 	arrData := []models.PhoneData{}
// 	ab := helpers.GetAllDataFromMongoDB()
// 	for i := 0; i < len(ab); i++ {
// 		// dataSpec := strings.Split(ab[i].Spesification, "\n")
// 		a := ab[12].Spesification
// 		// fmt.Println(a)
// 		// b := strings.Index(a, "Sound")
// 		// fmt.Println(b)
// 		// ca := strings.TrimSpace(ab[i].Spesification)
// 		// b := strings.ReplaceAll(ca, "Network", "|||")
// 		// c := strings.Index(ab[i].Spesification, strings.Contains("newt"))

// 		//for row network
// 		data.ProductId = "101014"
// 		data.Title = ab[12].Title
// 		data.UrlPhoto = ab[12].Photo
// 		offset, length := helpers.GetIndexTwoString(a, "Network", "Launch")
// 		network := a[offset:length]
// 		offset, length = helpers.GetIndexTwoString(network, "Technology", "2G bands")
// 		data.Network.Technology = strings.Trim(strings.Trim(network[offset:length], "Technology"), "\n\t")
// 		offset, length = helpers.GetIndexTwoString(network, "2G bands", "3G bands")
// 		data.Network.TwoGBands = strings.Trim(strings.Trim(network[offset:length], "2G bands"), "\n\t")
// 		offset, length = helpers.GetIndexTwoString(network, "2G bands", "3G bands")
// 		data.Network.TwoGBands = strings.Trim(strings.Trim(network[offset:length], "2G bands"), "\n\t")
// 		offset, length = helpers.GetIndexTwoString(network, "3G bands", "4G bands")
// 		data.Network.ThreeGBands = strings.Trim(strings.Trim(network[offset:length], "3G bands"), "\n\t")
// 		offset, length = helpers.GetIndexTwoString(network, "4G bands", "Speed")
// 		data.Network.FourGBands = strings.Trim(strings.Trim(network[offset:length], "4G bands"), "\n\t")
// 		positionSpeed := strings.Index(network, "Speed")
// 		data.Network.Speed = strings.Trim(strings.Trim(network[positionSpeed:], "Speed"), "\n\t")
// 		// fmt.Println(data.Network.Speed)
// 		//end for row network, continue ...
// 		// ac := strings.Trim(strings.Trim(data.Network.Technology, "Network")

// 		//for row launch
// 		offset, length = helpers.GetIndexTwoString(a, "Launch", "Body")
// 		launch := a[offset:length]
// 		offset, length = helpers.GetIndexTwoString(launch, "Announced", "Status")
// 		data.Launch.Announced = strings.Trim(strings.Trim(launch[offset:length], "Announced"), "\n\t")
// 		positionStatus := strings.Index(launch, "Status")
// 		data.Launch.Status = strings.Trim(strings.Trim(launch[positionStatus:], "Status"), "\n\t")

// 		//end for row launch, continue ...

// 		//for row body
// 		offset, length = helpers.GetIndexTwoString(a, "Body", "Display")
// 		body := a[offset:length]
// 		offset, length = helpers.GetIndexTwoString(body, "Dimensions", "Weight")
// 		data.Body.Dimensions = strings.Trim(strings.Trim(body[offset:length], "Dimensions"), "\n\t")
// 		offset, length = helpers.GetIndexTwoString(body, "Weight", "Build")
// 		data.Body.Weight = strings.Trim(strings.Trim(body[offset:length], "Weight"), "\n\t")
// 		offset, length = helpers.GetIndexTwoString(body, "Build", "SIM")
// 		data.Body.Build = strings.Trim(strings.Trim(body[offset:length], "Build"), "\n\t")
// 		positionSim := strings.Index(body, "SIM")
// 		data.Body.Sim = strings.Trim(strings.Trim(body[positionSim:], "SIM"), "\n\t")
// 		//end for row body, continue ..

// 		//for row Display
// 		offset, length = helpers.GetIndexTwoString(a, "Display", "Platform")
// 		display := a[offset:length]
// 		offset, length = helpers.GetIndexTwoString(display, "Type", "Size")
// 		data.Display.Type = strings.Trim(strings.Trim(display[offset:length], "Type"), "\n\t")
// 		offset, length = helpers.GetIndexTwoString(display, "Size", "Resolution")
// 		data.Display.Size = strings.Trim(strings.Trim(display[offset:length], "Size"), "\n\t")
// 		positionResolution := strings.Index(display, "Resolution")
// 		data.Display.Resolution = strings.Trim(strings.Trim(display[positionResolution:], "Resolution"), "\n\t")
// 		//end for row display, continue ...

// 		//for row platform
// 		offset, length = helpers.GetIndexTwoString(a, "Platform", "Memory")
// 		platform := a[offset:length]
// 		offset, length = helpers.GetIndexTwoString(platform, "OS", "Chipset")
// 		data.Platform.Os = strings.Trim(strings.Trim(platform[offset:length], "OS"), "\n\t")
// 		offset, length = helpers.GetIndexTwoString(platform, "Chipset", "CPU")
// 		data.Platform.Chipset = strings.Trim(strings.Trim(platform[offset:length], "Chipset"), "\n\t")
// 		offset, length = helpers.GetIndexTwoString(platform, "CPU", "GPU")
// 		data.Platform.Cpu = strings.Trim(strings.Trim(platform[offset:length], "CPU"), "\n\t")
// 		positionGPU := strings.Index(platform, "GPU")
// 		data.Platform.Gpu = strings.Trim(strings.Trim(platform[positionGPU:], "GPU"), "\n\t")
// 		//end for row platform

// 		// for row Memory
// 		offset, length = helpers.GetIndexTwoString(a, "Memory", "Main Camera")
// 		memory := a[offset:length]
// 		offset, length = helpers.GetIndexTwoString(memory, "Card slot", "Internal")
// 		data.Memory.CardSlot = strings.Trim(strings.Trim(memory[offset:length], "Card slot"), "\n\t")
// 		positionInternal := strings.Index(memory, "Internal")
// 		data.Memory.Internal = strings.Trim(strings.Trim(memory[positionInternal:], "Internal"), "\n\t")
// 		//end for row Memory, continue ...

// 		// for row Main Camera
// 		offset, length = helpers.GetIndexTwoString(a, "Main Camera", "Selfie camera")
// 		mainCamera := a[offset:length]
// 		if strings.Contains(mainCamera, "Triple") == true {
// 			offset, length = helpers.GetIndexTwoString(mainCamera, "Triple", "Features")
// 			data.MainCamera.Triple = strings.Trim(strings.Trim(mainCamera[offset:length], "Triple"), "\n\t")
// 		}
// 		if strings.Contains(mainCamera, "Quad") == true {
// 			offset, length = helpers.GetIndexTwoString(mainCamera, "Quad", "Features")
// 			data.MainCamera.Quad = strings.Trim(strings.Trim(mainCamera[offset:length], "Quad"), "\n\t")
// 		}
// 		offset, length = helpers.GetIndexTwoString(mainCamera, "Features", "Video")
// 		data.MainCamera.Features = strings.Trim(strings.Trim(mainCamera[offset:length], "Features"), "\n\t")
// 		positionVideo := strings.Index(mainCamera, "Video")
// 		data.MainCamera.Video = strings.Trim(strings.Trim(mainCamera[positionVideo:], "Video"), "\n\t")
// 		//end for row Main Camera, continue ...

// 		//for row Selfie Camera

// 		offset, length = helpers.GetIndexTwoString(a, "Selfie camera", "Sound")
// 		selfieCamera := a[offset:length]
// 		offset, length = helpers.GetIndexTwoString(selfieCamera, "Single", "Features")
// 		data.SelfieCamera.Single = strings.Trim(strings.Trim(selfieCamera[offset:length], "Single"), "\n\t")
// 		offset, length = helpers.GetIndexTwoString(selfieCamera, "Features", "Video")
// 		data.SelfieCamera.Features = strings.Trim(strings.Trim(selfieCamera[offset:length], "Features"), "\n\t")
// 		positionVideos := strings.Index(selfieCamera, "Video")
// 		data.SelfieCamera.Video = strings.Trim(strings.Trim(selfieCamera[positionVideos:], "Video"), "\n\t")

// 		// end for row Selfie Camera, continue...

// 		//for row Sound
// 		offset, length = helpers.GetIndexTwoString(a, "Sound", "Comms")
// 		sound := a[offset:length]
// 		offset, length = helpers.GetIndexTwoString(sound, "Loudspeaker", "3.5mm jack")
// 		data.Sound.Loudspeaker = strings.Trim(strings.Trim(sound[offset:length], "Loudspeaker"), "\n\t")
// 		positionJack := strings.Index(sound, "3.5mm jack")
// 		data.Sound.IsJack = strings.Trim(strings.Trim(sound[positionJack:], "3.5mm jack"), "\n\t")
// 		//end for row sound, continue ...

// 		// mixed row from comms before misc
// 		// for data row comms
// 		offset, length = helpers.GetIndexTwoString(a, "Comms", "Misc")
// 		mixedData := a[offset:length]
// 		offset, length = helpers.GetIndexTwoString(mixedData, "WLAN", "Bluetooth")
// 		data.Comms.Wlan = strings.Trim(strings.Trim(mixedData[offset:length], "WLAN"), "\n\t")
// 		offset, length = helpers.GetIndexTwoString(mixedData, "Bluetooth", "GPS")
// 		data.Comms.Bluetooth = strings.Trim(strings.Trim(mixedData[offset:length], "Bluetooth"), "\n\t")
// 		offset, length = helpers.GetIndexTwoString(mixedData, "GPS", "NFC")
// 		data.Comms.Gps = strings.Trim(strings.Trim(mixedData[offset:length], "GPS"), "\n\t")
// 		offset, length = helpers.GetIndexTwoString(mixedData, "NFC", "Radio")
// 		data.Comms.Nfc = strings.Trim(strings.Trim(mixedData[offset:length], "NFC"), "\n\t")
// 		offset, length = helpers.GetIndexTwoString(mixedData, "Radio", "USB")
// 		data.Comms.Radio = strings.Trim(strings.Trim(mixedData[offset:length], "Radio"), "\n\t")
// 		offset, length = helpers.GetIndexTwoString(mixedData, "USB", "Features")
// 		data.Comms.Radio = strings.Trim(strings.Trim(mixedData[offset:length], "USB"), "\n\t")
// 		//end data row comms

// 		//for data row Features
// 		offset, length = helpers.GetIndexTwoString(mixedData, "Sensors", "Battery")
// 		data.Features.Sensors = strings.Trim(strings.Trim(mixedData[offset:length], "Sensors"), "\n\t")
// 		//end data row features, continue ...

// 		//for data row battery
// 		offset, length = helpers.GetIndexTwoString(mixedData, "Type", "Charging")
// 		data.Battery.Type = strings.Trim(strings.Trim(mixedData[offset:length], "Type"), "\n\t")
// 		positionChargingstr := strings.Index(mixedData, "Charging")
// 		data.Battery.Charging = strings.Trim(strings.Trim(mixedData[positionChargingstr:], "Charging"), "\n\t")

// 		//end data row misc.

// 		//for data row misc
// 		positionMiscstr := strings.Index(a, "Misc")
// 		miscData := a[positionMiscstr:]
// 		offset, length = helpers.GetIndexTwoString(miscData, "Colors", "Models")
// 		data.Misc.Colors = strings.Trim(strings.Trim(miscData[offset:length], "Colors"), "\n\t")
// 		if strings.Contains(miscData, "SAR EU") == true {
// 			offset, length = helpers.GetIndexTwoString(miscData, "Models", "SAR EU")
// 			data.Misc.Models = strings.Trim(strings.Trim(miscData[offset:length], "Models"), "\n\t")
// 			offset, length = helpers.GetIndexTwoString(miscData, "SAR EU", "Price")
// 			data.Misc.SarEU = strings.Trim(strings.Trim(miscData[offset:length], "SAR EU"), "\n\t")
// 		} else {
// 			offset, length = helpers.GetIndexTwoString(miscData, "Models", "Price")
// 			data.Misc.Models = strings.Trim(strings.Trim(miscData[offset:length], "Models"), "\n\t")
// 		}
// 		positionPriceStr := strings.Index(miscData, "Price")
// 		data.Misc.Price = strings.Trim(strings.Trim(miscData[positionPriceStr:], "Price"), "\n\t")

// 		arrData = append(arrData, data)
// 		// //harus divalidasi
// 		// offset, length = helpers.GetIndexTwoString(a, "Features", "Battery")
// 		// data.Features = a[offset:length]
// 		// offset, length = helpers.GetIndexTwoString(a, "Battery", "Misc")
// 		// data.Battery = a[offset:length]
// 		// offsetMisc := strings.Index(a, "Misc")
// 		// data.Misc = a[offsetMisc:]
// 		// fmt.Println(ac)
// 		// fmt.Println(data.Network)

// 		// ad, _ := json.Marshal(arrData)
// 		// fmt.Println(string(ad))
// 	}
// 	ctx := context.Background()
// 	connect := new(models.DatabaseMongo)
// 	sess, err := connect.ConnectMongoDB()
// 	if err != nil {
// 		fmt.Println("Cannot connect to MongoDB")
// 	}
// 	defer sess.Disconnect(ctx)
// 	context := context.Background()
// 	var collection = sess.Database("product").Collection("real_spesificationProduct")
// 	collection.InsertOne(context, arrData[0])

// }
