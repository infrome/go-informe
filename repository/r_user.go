package repository

import (
	"database/sql"
	"fmt"
	"go-informe/models"
	"net"
	"os"
	"regexp"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
)

var (
	tokenSecret = []byte(os.Getenv("TOKEN_SECRET"))
)

func (r *repository) Register(u *models.User, conn *sql.DB) error {
	if len(u.Password) < 4 || len(u.PasswordConfirm) < 4 {
		return fmt.Errorf("Password must be at least 4 characters long.")
	}
	if len(u.UserName) < 0 {
		return fmt.Errorf("UserName Must Be Filled")
	}
	if u.Password != u.PasswordConfirm {
		return fmt.Errorf("Passwords do not match.")
	}

	if len(u.Email) < 4 {
		return fmt.Errorf("Email must be at least 4 characters long.")
	}

	u.Email = strings.ToLower(u.Email)
	row := conn.QueryRow("SELECT user_name,email FROM user_account where email = ? OR user_name = ?", u.Email, u.UserName)
	userLookup := models.User{}
	_ = row.Scan(&userLookup.Email, &userLookup.UserName)

	if userLookup.Email == u.Email {
		return fmt.Errorf("Email already exists! please choose another email")
	}
	if userLookup.UserName == u.UserName {
		return fmt.Errorf("UserName already exists! please choose another UserName")
	}
	pwdHash, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	if err != nil {
		return fmt.Errorf("There was an error creating your account.")
	}
	u.PasswordHash = string(pwdHash)

	_, err = conn.Exec("INSERT INTO user_account (created_at,user_name, email, password_hash) VALUES(?,?, ?, ?)", time.Now(), u.UserName, u.Email, u.PasswordHash)
	if err != nil {
		return fmt.Errorf("There was an error creating your account.")
	}

	return err
}

func (r *repository) GetAuthToken(u *models.User) (string, error) {
	claims := jwt.MapClaims{}
	claims["authorized"] = true
	claims["user_id"] = u.ID
	claims["exp"] = time.Now().Add(time.Hour * 24).Unix()
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, &claims)
	authToken, err := token.SignedString(tokenSecret)
	return authToken, err
}

func (r *repository) IsAuthenticated(u *models.User, conn *sql.DB) (string, string, string, string, error) {
	row := conn.QueryRow("SELECT id, password_hash from user_account WHERE email = ?", u.Email)
	err := row.Scan(&u.ID, &u.PasswordHash)
	if err != nil {
		fmt.Println("User with email not found")
		return "", "", "", "", fmt.Errorf("Invalid login credentials")
	}

	err = bcrypt.CompareHashAndPassword([]byte(u.PasswordHash), []byte(u.Password))
	if err != nil {
		return "", "", "", "", fmt.Errorf("Invalid login credentials")
	}

	row = conn.QueryRow("SELECT user_name, role from user_account WHERE email = ?", u.Email)
	err = row.Scan(&u.UserName, &u.Role)
	if err != nil {
		fmt.Println("role not found")
		return "", "", "", "", fmt.Errorf("Invalid login credentials")
	}

	return u.ID, u.UserName, u.Email, u.Role, nil
}

func (r *repository) GetPersonalInformationUser(userId string, conn *sql.DB) (interface{}, error) {
	data := make([]interface{}, 0)
	query := "select id,email,password_hash  from user_account where id =?"
	row, err := conn.Query(query, userId)
	if err != nil {
		return nil, err
	}
	for row.Next() {
		dataUser := models.UserInfromation{}
		err = row.Scan(&dataUser.ID, &dataUser.Email, &dataUser.PasswordHash)
		data = append(data, dataUser)
	}
	return data, nil
}
func (r *repository) UpdateUserInformation(userId string, conn *sql.DB) interface{} {
	query := "UPDATE user_account set email = ?,password_hash =? where id =?"
	_, err := conn.Exec(query, userId)
	if err != nil {
		return err.Error()
	}
	return nil
}

func (r *repository) InsertWishListUser(wishlist *models.StorageWishListUser, conn *sql.DB) error {
	_, err := conn.Exec("INSERT INTO wishlist(id,product_id) values(?,?) ", wishlist.Id, wishlist.ProductId)
	return err
}
func (r *repository) GetWishListUser(userId string, conn *sql.DB) (interface{}, error) {
	data := make([]interface{}, 0)
	dataWishlist := models.StorageWishListUser{}
	arrDataWishlist := []models.StorageWishListUser{}
	query := "select * from wishlist where id =?"
	row, err := conn.Query(query, userId)
	if err != nil {
		return nil, err
	}
	for row.Next() {
		err = row.Scan(&dataWishlist.Id, &dataWishlist.ProductId, &dataWishlist.WishId)
		arrDataWishlist = append(arrDataWishlist, dataWishlist)
	}
	fmt.Println("arrDataWishlist ", arrDataWishlist)
	for i := 0; i < len(arrDataWishlist); i++ {
		prodData := r.GetDataProductByProductId(arrDataWishlist[i].ProductId)
		data = append(data, prodData)
	}
	return data, nil
}
func (r *repository) DeleteWishListUser(userId, productId string, conn *sql.DB) (interface{}, error) {
	response := models.Response{}
	query := "Delete from wishlist where id = ? and product_id = ?"
	_, err := conn.Exec(query, userId, productId)
	if err != nil {
		response.Messages = append(response.Messages, err.Error())
		return response, nil
	}
	response.Messages = append(response.Messages, "sukses delete wishlist !")
	return response, nil
}

func (r *repository) EditProfile(reqParam *models.EditProfileParam, conn *sql.DB) interface{} {
	response := models.Response{}

	if reqParam.Email != "" {
		isValid := CheckEmail(reqParam.Email)
		if isValid == true {
			query := "UPDATE user_account SET email = ? WHERE id = ?"
			_, err := conn.Exec(query, reqParam.Email, reqParam.UserId)
			if err != nil {
				response.Messages = append(response.Messages, err.Error())
				response.Errors = err
				return response
			}
		} else {
			response.Messages = append(response.Messages, "EMAIL IS NOT VALID")
			return response
		}

	}

	if reqParam.Password != "" {
		if len(reqParam.Password) < 4 {
			response.Messages = append(response.Messages, "Password Length is Less Than 4")
			return response
		}
		pwdHash, err := bcrypt.GenerateFromPassword([]byte(reqParam.Password), bcrypt.DefaultCost)
		if err != nil {
			return fmt.Errorf("There was an error updating your account")
		}
		passwordHash := string(pwdHash)
		query := "UPDATE user_account SET password_hash = ? WHERE id = ?"
		_, err = conn.Exec(query, passwordHash, reqParam.UserId)
		if err != nil {
			response.Messages = append(response.Messages, err.Error())
			response.Errors = err
			return response
		}
	}

	if reqParam.UserName != "" {
		query := "UPDATE user_account SET user_name = ? WHERE id = ?"
		_, err := conn.Exec(query, reqParam.UserName, reqParam.UserId)
		if err != nil {
			response.Messages = append(response.Messages, err.Error())
			response.Errors = err
			return response
		}
	}

	return nil
}

func CheckEmail(e string) bool {
	var emailRegex = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

	if len(e) < 3 && len(e) > 254 {
		return false
	}
	if !emailRegex.MatchString(e) {
		return false
	}
	parts := strings.Split(e, "@")
	mx, err := net.LookupMX(parts[1])
	if err != nil || len(mx) == 0 {
		return false
	}
	return true
}
