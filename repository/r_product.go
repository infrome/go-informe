package repository

import (
	"bytes"
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"go-informe/models"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gocolly/colly"
	"github.com/gocolly/colly/debug"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gopkg.in/mgo.v2/bson"
)

func (r *repository) GetCategoryProduct() interface{} {
	connect := new(models.DatabaseMongo)
	data := []models.RespListCategory{}
	ctx := context.TODO()
	session, err := connect.ConnectMongoDB()
	findOption := options.Find()
	if err != nil {
		fmt.Println("Cannot connect to MongoDB")
	}
	defer session.Disconnect(ctx)
	var collection = session.Database("product").Collection("categoryProduct")
	// if keyword ==
	find, err := collection.Find(ctx, bson.M{}, findOption)

	err = find.All(ctx, &data)
	return data
}

func (r *repository) InsertComment(c *models.Comment, conn *sql.DB) interface{} {
	a, _ := json.Marshal(c)
	fmt.Println(string(a))
	// query := ""
	_, err := conn.Exec("INSERT INTO discussion(user_id, product_id, comment) VALUES(?, ?, ?)", c.Userid, c.Productid, c.Comment)
	if err != nil {
		return err.Error()
	}

	return nil
}

func (r *repository) InsertReplyComment(c *models.ReplyComment, conn *sql.DB) interface{} {
	_, err := conn.Exec("INSERT INTO reply_discussion(user_id, discussion_id, reply_comment) VALUES(?, ?, ?)", c.UserId, c.DiscussionId, c.ReplyComment)
	if err != nil {
		return err.Error()
	}

	return nil
}
func (r *repository) InsertReviewProduct(review *models.Review, conn *sql.DB) interface{} {

	_, err := conn.Exec("INSERT INTO review(product_id,review_name,review_comment,star) VALUES(?,?,?,?)", review.Productid, review.ReviewName, review.ReviewComment, review.Star)
	if err != nil {
		return err.Error()
	}

	return nil
}
func (r *repository) GetParentsCommentByProductID(productId, limit, offset string, conn *sql.DB) models.GetAllCommentJSON {
	//dataComment := []models.GetParentJson{}
	result := models.GetAllCommentJSON{}
	var iSummary []interface{}
	defaultData := []string{}
	iSummary = make([]interface{}, len(defaultData))
	fmt.Println("productId ", productId)
	fmt.Println("limit ", limit)
	fmt.Println("offset ", offset)
	query := "select discussion_id, user_id, comment from discussion where product_id = " + productId + " ORDER BY discussion_id DESC limit " + limit + " offset " + offset
	rows, err := conn.Query(query)
	fmt.Println("query ", query)
	count := 0
	if err != nil {
		fmt.Println("ERROR GET PARENTS COMMENT BY PRODUCT ID : " + err.Error())
		return result
	}
	for rows.Next() {
		each := models.GetParentJson{}
		rows.Scan(&each.DiscussionID, &each.UserId, &each.Comment)
		replies := GetReplyComment(each.DiscussionID, conn)
		name := GetUsernameByID(each.UserId, conn)
		comment := models.CommentJSON{
			DiscussionID: each.DiscussionID,
			Name:         name,
			Comment:      each.Comment,
			Replies:      replies,
		}

		count++
		iSummary = append(iSummary, comment)
	}
	result.Comment = iSummary
	result.Total = count
	return result

}
func (r *repository) GetTotalDataComment(productId string, conn *sql.DB) models.GetCountData {
	countData := models.GetCountData{}
	query := "select count(*) from discussion where product_id = ?"
	row, err := conn.Query(query, productId)
	if err != nil {
	}
	for row.Next() {
		err = row.Scan(&countData.CountData)
	}
	return countData
}
func (r *repository) GetRepliesCommentByProductId(productId string, conn *sql.DB) []models.GetComment {
	dataComment := []models.GetComment{}
	query := "select replyto_user_id,reply_comment,reply_name from reply_discussion where product_id = ?"
	row, err := conn.Query(query, productId)
	commentProduct := models.GetComment{}
	if err != nil {
	}
	for row.Next() {
		err = row.Scan(&commentProduct.ReplyTouserId, &commentProduct.ReplyComment, &commentProduct.ReplyName)
		dataComment = append(dataComment, commentProduct)
	}
	return dataComment

}
func (r *repository) GetAllReviewByPorductID(params models.RequestGetAllReview, conn *sql.DB) interface{} {
	reviewData := make([]interface{}, 0)
	reviewProduct := models.GetReview{}
	calcAvgStar := models.CalculateAverageStar{}
	countData := models.GetCountData{}
	var totalStar float64 = 0
	var totalData float64 = 0
	query := "select review_name,review_comment,star from review where product_id = ? limit ? offset ? "
	row, err := conn.Query(query, params.ProductId, params.Limit, params.Offset)
	if err != nil {
		return err.Error()
	}
	for row.Next() {
		err = row.Scan(&reviewProduct.ReviewName, &reviewProduct.ReviewComment, &reviewProduct.Star)
		reviewData = append(reviewData, reviewProduct)
	}
	for _, review := range reviewData {
		if review != nil {
			totalStar = totalStar + float64(reviewProduct.Star)
			totalData++
		}
	}
	query = "select count(*) from review where product_id = ? "
	row, err = conn.Query(query, params.ProductId)
	if err != nil {
		return err.Error()
	}
	for row.Next() {
		err = row.Scan(&countData.CountData)
	}
	calcAvgStar.AverageStar = totalStar / totalData
	reviewData = append(reviewData, calcAvgStar)
	reviewData = append(reviewData, countData)
	return reviewData

}
func (r *repository) GetAllReplyCommentByProductID(productId string, conn *sql.DB) interface{} {
	comment := make([]interface{}, 0)
	query := "select comment,reply_comment from discussion ds left join reply_discussion rd on ds.product_id = rd.product_id where ds.product_id = ?"
	row, err := conn.Query(query, productId)
	if err != nil {
	}
	for row.Next() {
		commentProduct := models.GetComment{}
		err = row.Scan(&commentProduct.Comment, &commentProduct.ReplyComment)
		comment = append(comment, commentProduct)
	}
	return comment

}
func (r *repository) GetProductDetail(keyword string) []models.ScrapNewData {
	connect := new(models.DatabaseMongo)
	data := []models.ScrapNewData{}
	ctx := context.TODO()
	session, err := connect.ConnectMongoDB()
	findOption := options.Find()
	if err != nil {
		fmt.Println("Cannot connect to MongoDB")
	}
	defer session.Disconnect(ctx)
	var collection = session.Database("product").Collection("spesificationProduct")
	// if keyword ==
	find, err := collection.Find(ctx, bson.M{"product_id": keyword}, findOption)

	err = find.All(ctx, &data)
	return data
}
func (r *repository) SearchNavbarProduct(keyword string) []models.ScrapNewData {
	connect := new(models.DatabaseMongo)
	data := []models.ScrapNewData{}
	ctx := context.TODO()
	session, err := connect.ConnectMongoDB()
	findOption := options.Find()
	if err != nil {
		fmt.Println("Cannot connect to MongoDB")
	}
	defer session.Disconnect(ctx)
	var collection = session.Database("product").Collection("spesificationProduct")
	// if keyword ==
	find, err := collection.Find(ctx, bson.M{"title": primitive.Regex{Pattern: "^.*" + keyword + ".*", Options: "i"}}, findOption)

	err = find.All(ctx, &data)
	return data
}

func (r *repository) GetAllDataProduct() []models.ScrapNewData {
	connect := new(models.DatabaseMongo)
	data := []models.ScrapNewData{}
	ctx := context.TODO()
	session, err := connect.ConnectMongoDB()
	findOption := options.Find()
	if err != nil {
		fmt.Println("Cannot connect to MongoDB")
	}
	defer session.Disconnect(ctx)
	var collection = session.Database("product").Collection("spesificationProduct")
	find, err := collection.Find(ctx, bson.M{}, findOption)

	err = find.All(ctx, &data)
	return data
}

func (r *repository) GetEcommerceData(reqParams *models.ReqDataToEcommerce) []models.GetEcommerceData {
	connect := new(models.DatabaseMongo)
	data := []models.GetEcommerceData{}
	ctx := context.TODO()
	session, err := connect.ConnectMongoDB()
	findOption := options.Find()
	if err != nil {
		fmt.Println("Cannot connect to MongoDB")
	}
	defer session.Disconnect(ctx)
	var collection = session.Database("product").Collection("ecommerce_data")
	find, err := collection.Find(ctx, bson.M{"product_id": reqParams.ProductID}, findOption)
	if err != nil {
		fmt.Println("error find syntax ", find, err)
	}

	err = find.All(ctx, &data)
	if err != nil {
		fmt.Println("error find all ", err)
	}
	fmt.Println("data ", data)

	return data
}
func (r *repository) GetListDataByCategory(reqParams *models.ReqListDataByCategory) interface{} {
	response := make([]interface{}, 0)
	connect := new(models.DatabaseMongo)
	// if reqParams.CategoryProduct == "Laptop" {
	// data := []models.LaptopData{}
	data := []models.ScrapNewData{}
	ctx := context.TODO()
	session, err := connect.ConnectMongoDB()
	findOption := options.Find()
	if err != nil {
		fmt.Println("Cannot connect to MongoDB")
	}
	defer session.Disconnect(ctx)
	var collection = session.Database("product").Collection("spesificationProduct")
	// if keyword ==
	find, err := collection.Find(ctx, bson.M{"category": reqParams.CategoryProduct}, findOption)

	err = find.All(ctx, &data)
	response = append(response, data)
	// } else if reqParams.CategoryProduct == "Handphone" {
	// 	data := []models.PhoneData{}
	// 	ctx := context.TODO()
	// 	session, err := connect.ConnectMongoDB()
	// 	findOption := options.Find()
	// 	if err != nil {
	// 		fmt.Println("Cannot connect to MongoDB")
	// 	}
	// 	defer session.Disconnect(ctx)
	// 	var collection = session.Database("product").Collection("real_spesificationProduct")
	// 	// if keyword ==
	// 	find, err := collection.Find(ctx, bson.M{"category_product": reqParams.CategoryProduct}, findOption)

	// 	err = find.All(ctx, &data)
	// 	response = append(response, data)
	// }

	return response
}

func (r *repository) GetRecommendUpgradeProduct(reqParams *models.ReqUpgradeProduct) []models.PhoneData {
	connect := new(models.DatabaseMongo)
	data := []models.GetEcommerceData{}
	ctx := context.TODO()
	session, err := connect.ConnectMongoDB()
	findOption := options.Find()
	if err != nil {
		fmt.Println("Cannot connect to MongoDB")
	}
	defer session.Disconnect(ctx)
	var collection = session.Database("product").Collection("ecommerce_data")
	find, err := collection.Find(ctx, bson.M{}, findOption)
	err = find.All(ctx, &data)
	tempPrice := ""
	// for i := 0; i < len(data); i++ {
	// 	if data[i].ProductId == reqParams.ProductId {
	// 		convPrice, _ := strconv.Atoi(data[i].Price)
	// 		a, _ := sort.Sort(data)
	// 	}

	// }
	fmt.Println(tempPrice)

	return nil
}

func GetReplyComment(discussionId int, conn *sql.DB) interface{} {

	result := []models.ResultReplyCommentJSON{}
	query := `SELECT reply_discussion_id, user_id, reply_comment
				FROM reply_discussion
				WHERE discussion_id = ? ORDER BY reply_discussion_id ASC`
	rows, err := conn.Query(query, discussionId)
	if err != nil {
		fmt.Println("ERROR GET REPLY COMMENT : ", err.Error())
		return nil
	}

	for rows.Next() {
		each := models.ResultReplyComment{}
		rows.Scan(&each.ReplyID, &each.UserID, &each.ReplyComment)
		name := GetUsernameByID(each.UserID, conn)
		eachJSON := models.ResultReplyCommentJSON{
			ReplyID:      each.ReplyID,
			Name:         name,
			ReplyComment: each.ReplyComment,
		}
		result = append(result, eachJSON)
	}

	return result
}

func GetUsernameByID(userID int, conn *sql.DB) string {
	result := ""
	query := `SELECT user_name
				FROM user_account
				WHERE id = ?`
	rows, err := conn.Query(query, userID)
	if err != nil {
		fmt.Println("ERROR GET USERNAME : ", err.Error())
		return result
	}

	for rows.Next() {
		each := models.ResultGetUsername{}
		rows.Scan(&each.Name)
		result = each.Name
	}

	return result
}

func (*repository) DeleteComment(reqParams *models.DeleteCommentParam) interface{} {
	model := new(models.DatabaseSql)
	conn := model.ConnectSql()
	replyId := strconv.Itoa(reqParams.ReplyDiscussionID)
	discussId := strconv.Itoa(reqParams.DiscussionID)

	if reqParams.ReplyDiscussionID > 0 {
		query := `DELETE FROM reply_discussion
					WHERE reply_discussion_id = ? AND discussion_id = ?`
		_, err := conn.Exec(query, replyId, discussId)
		if err != nil {
			fmt.Println("ERROR DELETE COMMENT :", err.Error())
			return err
		}
	} else {
		query := `DELETE FROM discussion
					WHERE discussion_id = ?`
		_, err := conn.Exec(query, discussId)
		if err != nil {
			fmt.Println("ERROR DELETE COMMENT :", err.Error())
			return err
		}

		query = `DELETE FROM reply_discussion
					WHERE discussion_id = ?`
		_, err = conn.Exec(query, discussId)
		if err != nil {
			fmt.Println("ERROR DELETE COMMENT :", err.Error())
			return err
		}
	}
	return nil
}
func (*repository) UpgradeProduct(reqParams *models.UpgradeProductParam) (interface{}, error) {
	upgradePhone := []models.ScrapNewData{}
	connect := new(models.DatabaseMongo)
	scrapNewData := []models.ScrapNewData{}
	ctx := context.TODO()
	session, err := connect.ConnectMongoDB()
	findOption := options.Find()
	if err != nil {
		fmt.Println("Cannot connect to MongoDB")
		return upgradePhone, err
	}
	defer session.Disconnect(ctx)
	var collection = session.Database("product").Collection("spesificationProduct")
	// if keyword ==
	find, err := collection.Find(ctx, bson.M{"product_id": reqParams.ProductID}, findOption)
	err = find.All(ctx, &scrapNewData)
	if err != nil {
		fmt.Println("ERROR GET DATA UPGRADE PRODUCT : ", err.Error())
		return upgradePhone, err
	}

	allPhone := []models.ScrapNewData{}

	find, err = collection.Find(ctx, bson.M{}, findOption)
	err = find.All(ctx, &allPhone)
	if err != nil {
		fmt.Println("ERROR GET DATA UPGRADE PRODUCT : ", err.Error())
		return upgradePhone, err
	}
	for _, item := range allPhone {
		if item.LowestPrice <= reqParams.Budget && item.LowestPrice >= scrapNewData[0].LowestPrice && item.Title != scrapNewData[0].Title && item.Category == scrapNewData[0].Category {
			upgradePhone = append(upgradePhone, item)
		}
	}
	return upgradePhone, nil
}

func (r *repository) GetPopularProduct() (interface{}, error) {
	connect := new(models.DatabaseMongo)
	allData := []models.ScrappingProductEcommerce{}
	ctx := context.TODO()
	session, err := connect.ConnectMongoDB()
	findOption := options.Find()
	if err != nil {
		fmt.Println("Cannot connect to MongoDB")
		return nil, err
	}
	findOption.SetSort(bson.M{"sold": -1})

	defer session.Disconnect(ctx)
	var collection = session.Database("product").Collection("ecommerce_data")
	find, err := collection.Find(ctx, bson.M{}, findOption)

	err = find.All(ctx, &allData)
	if err != nil {
		fmt.Println("error find all ", err)
		return nil, err
	}

	arrayProductId := []string{}
	for i, arrData := range allData {
		if len(arrayProductId) < 11 {
			if i == 0 {
				arrayProductId = append(arrayProductId, arrData.ProductId)
			}
			isExist := stringInSlice(arrData.ProductId, arrayProductId)
			if isExist == false {
				arrayProductId = append(arrayProductId, arrData.ProductId)
			}
		}
	}
	fmt.Println("arrayProductId ", arrayProductId)
	popularData := make([]interface{}, 0)
	for _, productId := range arrayProductId {
		fmt.Println("productId ", productId)
		// tempPhone := []models.TempPhoneData{}
		// tempLaptop := []models.TempLaptopData{}
		scrapNeewData := []models.ScrapNewData{}

		var collection = session.Database("product").Collection("spesificationProduct")
		find, err := collection.Find(ctx, bson.M{"product_id": productId}, findOption)
		if err != nil {
			fmt.Println("Fail to get data from mongo")
			return nil, err
		}
		// if productId[0:2] == "10" {
		err = find.All(ctx, &scrapNeewData)
		if err != nil {
			fmt.Println("No data found")
		}
		popularData = append(popularData, scrapNeewData)
		// } else {
		// 	err = find.All(ctx, &tempLaptop)
		// 	if err != nil {
		// 		fmt.Println("No data found")
		// 	}
		// 	popularData = append(popularData, tempLaptop)
		// }
	}

	return popularData, nil
}

func (*repository) GetProductPeopleChoose(reqParams *models.ProductPeopleChooseParam) (interface{}, error) {
	connect := new(models.DatabaseMongo)
	allData := []models.ScrappingProductEcommerce{}
	ctx := context.TODO()
	session, err := connect.ConnectMongoDB()
	findOption := options.Find()
	if err != nil {
		fmt.Println("Cannot connect to MongoDB")
		return nil, err
	}
	findOption.SetSort(bson.M{"sold": -1})

	defer session.Disconnect(ctx)
	var collection = session.Database("product").Collection("ecommerce_data")
	query := `^10`
	if reqParams.ProductID[0:2] == "10" {
		query = `^10`
	} else {
		query = `^20`
	}
	find, err := collection.Find(ctx, bson.M{"product_id": primitive.Regex{query, ""}}, findOption)

	err = find.All(ctx, &allData)
	if err != nil {
		fmt.Println("error find all ", err)
		return nil, err
	}

	arrayProductId := []string{}
	for i, arrData := range allData {
		if len(arrayProductId) < 11 {
			if arrData.ProductId != reqParams.ProductID {
				if i == 0 {
					arrayProductId = append(arrayProductId, arrData.ProductId)
				}
				isExist := stringInSlice(arrData.ProductId, arrayProductId)
				if isExist == false {
					arrayProductId = append(arrayProductId, arrData.ProductId)
				}
			}
		}
	}

	productPeopleChoose := make([]interface{}, 0)
	for _, productId := range arrayProductId {
		tempPhone := []models.TempPhoneData{}
		tempLaptop := []models.TempLaptopData{}

		var collection = session.Database("product").Collection("spesificationProduct")
		find, err := collection.Find(ctx, bson.M{"product_id": productId}, findOption)
		if err != nil {
			fmt.Println("Fail to get data from mongo")
			return nil, err
		}
		if productId[0:2] == "10" {
			err = find.All(ctx, &tempPhone)
			if err != nil {
				fmt.Println("No data found")
			}
			productPeopleChoose = append(productPeopleChoose, tempPhone)
		} else {
			err = find.All(ctx, &tempLaptop)
			if err != nil {
				fmt.Println("No data found")
			}
			productPeopleChoose = append(productPeopleChoose, tempLaptop)
		}
	}

	return productPeopleChoose, nil
}

func (r *repository) GetDataProductByProductId(productId string) []models.ScrapNewData {
	connect := new(models.DatabaseMongo)
	data := []models.ScrapNewData{}
	ctx := context.TODO()
	session, err := connect.ConnectMongoDB()
	findOption := options.Find()
	if err != nil {
		fmt.Println("Cannot connect to MongoDB")
	}
	defer session.Disconnect(ctx)
	var collection = session.Database("product").Collection("spesificationProduct")
	find, err := collection.Find(ctx, bson.M{"product_id": productId}, findOption)

	err = find.All(ctx, &data)
	return data
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func (r *repository) GetScrappingEcommerce(reqBody *models.ScrappingEcommerceParam) error {
	url := reqBody.Url
	web := reqBody.Web
	productId := reqBody.ProductID
	fmt.Println("url", url)
	fmt.Println("web", web)
	// escaped := regexp.QuoteMeta("bukalapak.com")
	// z := regexp.MustCompile(`^https?:\/\/[a-z]*\.?` + escaped + `.*`)
	c := colly.NewCollector(
		colly.Debugger(&debug.LogDebugger{}),
		colly.URLFilters(),
		// colly.UserAgent("xy"),
		colly.UserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36"),
		// colly.UserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36"),
		colly.Async(true),
		colly.AllowURLRevisit(),
		// colly.AllowedDomains("www.bukalapak.com", "bukalapak.com", "https://www.bukalapak.com", "https://www.bukalapak.com/", "shopee.co.id", "tokopedia.com"),
	)

	c.Limit(&colly.LimitRule{
		// DomainRegexp: "127.0.0.1:8000",
		// Parallelism:  2,
		Delay: 2 * time.Second,
	})

	// c.WithTransport(&http.Transport{
	// 	Proxy: http.ProxyFromEnvironment,
	// 	DialContext: (&net.Dialer{
	// 		Timeout:   30 * time.Second,
	// 		KeepAlive: 30 * time.Second,
	// 		DualStack: true,
	// 	}).DialContext,
	// 	MaxIdleConns:          100,
	// 	IdleConnTimeout:       90 * time.Second,
	// 	TLSHandshakeTimeout:   10 * time.Second,
	// 	ExpectContinueTimeout: 1 * time.Second,ScrappingProductEcommerce
	// })

	// c.WithTransport(&http.Transport{
	// 	DisableKeepAlives: true,
	// })

	productData := []models.ScrappingProductEcommerce{}
	prodData := models.ScrappingProductEcommerce{}
	if web == "tokopedia" {
		c.OnHTML(".css-7fmtuv", func(e *colly.HTMLElement) {
			fmt.Println("OnHTML tokopedia ", e.ChildText("div[data-testid=spnSRPProdName]"))
			prodData.Name = e.ChildText("div[data-testid=spnSRPProdName]")
			prodData.Price = e.ChildText("div[data-testid=spnSRPProdPrice]")
			prodData.Rating = e.ChildText(".css-etd83i")
			tempSold := e.ChildText(".css-1mv2cn2")
			prodData.ProductId = productId
			prodData.Store = e.ChildText("span[data-testid=spnSRPProdTabShopLoc]")
			prodData.UrlProduct = e.ChildAttr("a", "href")
			prodData.Web = "tokopedia"
			stringSold := strings.Replace(tempSold, "Terjual ", "", -1)
			intSold, err := strconv.Atoi(stringSold)
			prodData.Sold = intSold
			productData = append(productData, prodData)
			a, _ := json.Marshal(prodData)
			fmt.Println(string(a))

			// Print link
			connect := new(models.DatabaseMongo)
			ctx := context.Background()
			session, err := connect.ConnectMongoDB()
			if err != nil {
				fmt.Println("Cannot connect to MongoDB")
				// return err
			}
			defer session.Disconnect(ctx)
			var collection = session.Database("product").Collection("ecommerce_data")

			if prodData.Name != "" {
				findOption := options.Find()
				find, err := collection.Find(ctx, bson.M{"name": prodData.Name, "store": prodData.Store, "web": "tokopedia"}, findOption)
				if err != nil {
					fmt.Println("error find syntax ", find, err)
					// return err
				}
				data := []models.FindProductEcommerce{}
				err = find.All(ctx, &data)
				if err != nil {
					fmt.Println("error find all ", err)
					// return err
				}
				// fmt.Println("find ", data)
				if len(data) == 0 && prodData.Name != "" {
					fmt.Println("insert ", prodData.Name)
					insert, err := collection.InsertOne(ctx, prodData)
					if err != nil {
						fmt.Println("error insert ", insert, err)
						// return err
					}
				}
			}
		})
	} else if web == "shopee" {
		fmt.Println("masuk shopee")
		c.OnHTML("head", func(e *colly.HTMLElement) {

			//fmt.Println("OnHTML shopee ", e.ChildT("script"))
			prodData.Name = e.ChildText("._1NoI8_ A6gE1J _1co5xN")
			prodData.Price = e.ChildText("._1xk7ak")
			// prodData.Rating = e.ChildText(".css-etd83i")
			// prodData.Sold = e.ChildText("._245-SC")
			prodData.ProductId = productId
			prodData.Store = e.ChildText("._41f1_p")
			prodData.UrlProduct = e.ChildAttr("a", "href")
			prodData.Web = "shopee"
			productData = append(productData, prodData)
			a, _ := json.Marshal(prodData)
			fmt.Println(string(a))

			// Print link
			connect := new(models.DatabaseMongo)
			ctx := context.Background()
			session, err := connect.ConnectMongoDB()
			if err != nil {
				fmt.Println("Cannot connect to MongoDB")
				// return err
			}
			defer session.Disconnect(ctx)
			var collection = session.Database("product").Collection("ecommerce_data")

			if prodData.Name != "" {
				findOption := options.Find()
				find, err := collection.Find(ctx, bson.M{"name": prodData.Name, "store": prodData.Store, "web": "shopee"}, findOption)
				if err != nil {
					fmt.Println("error find syntax ", find, err)
					// return err
				}
				data := []models.FindProductEcommerce{}
				err = find.All(ctx, &data)
				if err != nil {
					fmt.Println("error find all ", err)
					// return err
				}
				// fmt.Println("find ", data)
				if len(data) == 0 && prodData.Name != "" {
					fmt.Println("insert ", prodData.Name)
					// insert, err := collection.InsertOne(ctx, prodData)
					// if err != nil {prodData.Sold
					// 	fmt.Println("error insert ", insert, err)
					// 	// return err
					// }
				}
			}
		})
	} else if web == "blibli" {
		fmt.Println("masuk blibli")
		urlBlibliFront := "https://www.blibli.com/backend/search/products?sort=0&page=1&start=0&searchTerm="
		product := strings.ReplaceAll(reqBody.ProductName, " ", "%20")
		urlBlibliBehind := "&intent=true&merchantSearch=true&multiCategory=true&customUrl=&&channelId=web&showFacet=true"
		urlBlibli := urlBlibliFront + product + urlBlibliBehind
		fmt.Println("URL BLIBLI : ", urlBlibli)
		data := RunScrap(urlBlibli)

		for _, item := range data.Data.Products {
			fmt.Println(item.Name)
			fmt.Println(item.Price.Price)

			saveMongo := models.ScrappingProductEcommerce{}
			saveMongo.ProductId = reqBody.ProductID
			saveMongo.Name = item.Name
			saveMongo.Price = item.Price.Price
			saveMongo.Rating = strconv.Itoa(item.Review.Rating)
			saveMongo.Sold = item.Review.Sold
			saveMongo.Store = "Jakarta"
			saveMongo.UrlProduct = "blibli.com" + item.URL
			saveMongo.Web = "blibli"

			connect := new(models.DatabaseMongo)
			ctx := context.Background()
			session, err := connect.ConnectMongoDB()
			if err != nil {
				fmt.Println("Cannot connect to MongoDB")
				// return err
			}
			defer session.Disconnect(ctx)
			var collection = session.Database("product").Collection("ecommerce_data")

			if saveMongo.Name != "" {
				findOption := options.Find()
				find, err := collection.Find(ctx, bson.M{"name": saveMongo.Name, "store": saveMongo.Store, "web": "blibli"}, findOption)
				if err != nil {
					fmt.Println("error find syntax ", find, err)
					return err
				}
				data := []models.FindProductEcommerce{}
				err = find.All(ctx, &data)
				if err != nil {
					fmt.Println("error find all ", err)
					return err
				}
				// fmt.Println("find ", data)
				if len(data) == 0 && saveMongo.Name != "" {
					fmt.Println("insert ", saveMongo.Name)
					insert, err := collection.InsertOne(ctx, saveMongo)
					if err != nil {
						fmt.Println("error insert ", insert, err)
						return err
					}
				}
			}

		}

		// c.OnHTML(".product__content", func(e *colly.HTMLElement) {
		// 	fmt.Println("OnHTML blibli ", e.ChildText("product__description"))
		// 	prodData.Name = e.ChildText(".product__title__name")
		// 	prodData.Price = e.ChildText(".product__body__price__display")
		// 	prodData.Rating = e.ChildText(".product__body__rating__stars__rating")
		// 	prodData.Sold = e.ChildText(".product__body__rating__stars__count")
		// 	prodData.ProductId = productId
		// 	prodData.Store = e.ChildText(".product__body__location__text")
		// 	// prodData.UrlProduct = e.ChildAttr("a", "href")
		// 	prodData.Web = "blibli"
		// 	productData = append(productData, prodData)
		// 	a, _ := json.Marshal(prodData)
		// 	fmt.Println("asdf ", string(a))

		// 	// Print link
		// 	connect := new(models.DatabaseMongo)
		// 	ctx := context.Background()
		// 	session, err := connect.ConnectMongoDB()
		// 	if err != nil {
		// 		fmt.Println("Cannot connect to MongoDB")
		// 		// return err
		// 	}
		// 	defer session.Disconnect(ctx)
		// 	var collection = session.Database("product").Collection("ecommerce_data")

		// 	findOption := options.Find()
		// 	find, err := collection.Find(ctx, bson.M{"name": prodData.Name, "store": prodData.Store, "web": "blibli"}, findOption)
		// 	if err != nil {
		// 		fmt.Println("error find syntax ", find, err)
		// 		// return err
		// 	}
		// 	data := []models.FindProductEcommerce{}
		// 	err = find.All(ctx, &data)
		// 	if err != nil {
		// 		fmt.Println("error find all ", err)
		// 		// return err
		// 	}
		// 	// fmt.Println("find ", data)
		// 	fmt.Println("productData ", productData)
		// 	if len(data) == 0 && prodData.Name != "" {
		// 		fmt.Println("insert ", prodData.Name)
		// 		// insert, err := collection.InsertOne(ctx, prodData)
		// 		// if err != nil {
		// 		// 	fmt.Println("error insert ", insert, err)
		// 		// 	// return err
		// 		// }
		// 	}

		// })
	} else if web == "bukalapak" {
		fmt.Println("masuk bukalapak")
		// c.OnRequest(func(r *colly.Request) {
		// 	r.Headers.Set("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36")
		// })
		c.OnHTML(".bl-product-card__description", func(e *colly.HTMLElement) {
			fmt.Println("OnHTML bukalapak ", e.ChildText(".bl-product-card__description-name"))
			prodData.Name = e.ChildText(".bl-product-card__description-name")
			prodData.Price = e.ChildText(".bl-product-card__description-price")
			prodData.Rating = e.ChildText(".bl-product-card__description-rating")
			// prodData.Sold = e.ChildText(".bl-product-card__separator")
			prodData.ProductId = productId
			prodData.Store = e.ChildText(".bl-product-card__store")
			prodData.UrlProduct = e.ChildAttr("a", "href")
			prodData.Web = "bukalapak"
			productData = append(productData, prodData)
			a, _ := json.Marshal(prodData)
			fmt.Println(string(a))

			// Print link
			connect := new(models.DatabaseMongo)
			ctx := context.Background()
			session, err := connect.ConnectMongoDB()
			if err != nil {
				fmt.Println("Cannot connect to MongoDB")
				// return err
			}
			defer session.Disconnect(ctx)
			var collection = session.Database("product").Collection("ecommerce_data")

			findOption := options.Find()
			find, err := collection.Find(ctx, bson.M{"name": prodData.Name, "store": prodData.Store, "web": "bukalapak"}, findOption)
			if err != nil {
				fmt.Println("error find syntax ", find, err)
				// return err
			}
			data := []models.FindProductEcommerce{}
			err = find.All(ctx, &data)
			if err != nil {
				fmt.Println("error find all ", err)
				// return err
			}
			// fmt.Println("find ", data)
			if len(data) == 0 && prodData.Name != "" {
				fmt.Println("insert ", prodData.Name)
				insert, err := collection.InsertOne(ctx, prodData)
				if err != nil {
					fmt.Println("error insert ", insert, err)
					// return err
				}
			}
		})
	}

	c.OnRequest(func(r *colly.Request) {
		// r.Headers.Set("Accept", "*/*")
		fmt.Println("visiting: ", url)
	})

	c.OnError(func(r *colly.Response, err error) {
		fmt.Println("error: ", err)
	})

	c.Visit(url)
	c.Wait()
	return nil
}

func RunScrap(url string) models.BlibliScrap {
	buffer := new(bytes.Buffer)

	timeout := time.Duration(30 * time.Second)
	client := &http.Client{
		Timeout: timeout,
	}

	req, err := http.NewRequest("GET", url, buffer)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "*/*")
	req.Header.Add("User-Agent", "PostmanRuntime/7.26.8")

	// helpers.DumpRequestPayment(req, "request_scrap.log")
	resp, err := client.Do(req)
	// var hasil []byte
	response := models.BlibliScrap{}
	//response := model.ResInquiryKredivo{}
	if err != nil {
		//rupaResp = helpers.ErrorMessage(rupaResp, "Inquiry Kredivo Failed", "Failed to check status", "request error", 500)
		return response
	} else {
		json.NewDecoder(resp.Body).Decode(&response)
		// hasil, _ = json.Marshal(response)

		// helpers.DumpResponsePayment(string(hasil), "request_scrap.log")
		defer resp.Body.Close()
	}
	return response
}

func (r *repository) ScrapAllEcommerce(reqBody *models.ScrapAllEcommerceParam) error {

	connect := new(models.DatabaseMongo)
	product := []models.PhoneScrap{}

	ctx := context.TODO()
	session, err := connect.ConnectMongoDB()
	findOption := options.Find()
	if err != nil {
		fmt.Println("Cannot connect to MongoDB")
	}
	defer session.Disconnect(ctx)
	fmt.Println("CATEGORY ", reqBody.Category)
	var collection = session.Database("product").Collection("spesificationProduct")
	find, err := collection.Find(ctx, bson.M{"category": reqBody.Category}, findOption)
	err = find.All(ctx, &product)
	if err != nil {
		return err
	}
	for _, item := range product {
		arrInt := []int{}
		fmt.Println(item.Title)
		test, _ := json.Marshal(item)
		fmt.Println(string(test))

		inTokped, _ := ScrapTokopedia(item.Category, item.Title, item.ProductID)
		inBlibli, _ := ScrapBlibli(item.Category, item.Title, item.ProductID)
		arrInt = append(inTokped, inBlibli...)
		min, _ := FindMininum(arrInt)

		fmt.Println("TITLE DI FUNC ", item.Title)
		res, _ := UpdateByProductId(min, item.ProductID)
		fmt.Println("MODIFIED COUNT : ", res.ModifiedCount)
		fmt.Println("MATCH COUNT : ", res.MatchedCount)
	}

	return nil
}

func ScrapTokopedia(category, name, productId string) ([]int, error) {
	arrInt := []int{}
	url := "https://www.tokopedia.com/search?navsource=home&st=product&q="
	product := strings.ReplaceAll(name, " ", "%20")
	tokpedCategory := ""
	if category == "Handphone" {
		tokpedCategory = "&sc=24"
	} else if category == "Laptop" {
		tokpedCategory = "&sc=3844"
	}
	tokpedURL := url + product + tokpedCategory
	fmt.Println(tokpedURL)
	c := colly.NewCollector(
		colly.Debugger(&debug.LogDebugger{}),
		colly.URLFilters(),

		colly.UserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36"),
		colly.Async(true),
		colly.AllowURLRevisit(),
	)

	c.Limit(&colly.LimitRule{
		Delay: 2 * time.Second,
	})

	productData := []models.ScrappingProductEcommerce{}
	prodData := models.ScrappingProductEcommerce{}

	c.OnHTML(".css-7fmtuv", func(e *colly.HTMLElement) {
		fmt.Println("OnHTML tokopedia ", e.ChildText("div[data-testid=spnSRPProdName]"))
		prodData.Name = e.ChildText("div[data-testid=spnSRPProdName]")
		prodData.Price = e.ChildText("div[data-testid=spnSRPProdPrice]")
		prodData.Rating = e.ChildText(".css-etd83i")
		tempSold := e.ChildText(".css-1mv2cn2")
		prodData.ProductId = productId
		prodData.Store = e.ChildText("span[data-testid=spnSRPProdTabShopLoc]")
		prodData.UrlProduct = e.ChildAttr("a", "href")
		prodData.Web = "tokopedia"
		stringSold := strings.Replace(tempSold, "Terjual ", "", -1)
		intSold, err := strconv.Atoi(stringSold)
		prodData.Sold = intSold
		productData = append(productData, prodData)
		a, _ := json.Marshal(prodData)
		fmt.Println(string(a))

		s := strings.Replace(prodData.Price, "Rp", "", -1)
		s = strings.Replace(s, ".", "", -1)
		in, _ := strconv.Atoi(s)
		fmt.Println("INT TOKPED ", in)
		if in > 0 {
			fmt.Println("MASUK NIH")
			arrInt = append(arrInt, in)
		}
		// Print link
		connect := new(models.DatabaseMongo)
		ctx := context.Background()
		session, err := connect.ConnectMongoDB()
		if err != nil {
			fmt.Println("Cannot connect to MongoDB")

		}
		defer session.Disconnect(ctx)
		var collection = session.Database("product").Collection("ecommerce_data")

		if prodData.Name != "" {
			findOption := options.Find()
			find, err := collection.Find(ctx, bson.M{"name": prodData.Name, "store": prodData.Store, "web": "tokopedia"}, findOption)
			if err != nil {
				fmt.Println("error find syntax ", find, err)

			}
			data := []models.FindProductEcommerce{}
			err = find.All(ctx, &data)
			if err != nil {
				fmt.Println("error find all ", err)

			}

			if len(data) == 0 && prodData.Name != "" {
				fmt.Println("insert ", prodData.Name)
				insert, err := collection.InsertOne(ctx, prodData)
				if err != nil {
					fmt.Println("error insert ", insert, err)

				}
			}
		}
	})

	c.OnRequest(func(r *colly.Request) {
		// r.Headers.Set("Accept", "*/*")
		fmt.Println("visiting: ", tokpedURL)
	})

	c.OnError(func(r *colly.Response, err error) {
		fmt.Println("error: ", err)
	})

	c.Visit(tokpedURL)
	c.Wait()
	return arrInt, nil
}

func ScrapBlibli(category, name, productId string) ([]int, error) {
	arrInt := []int{}
	fmt.Println("Scraping blibli...")
	urlBlibliFront := "https://www.blibli.com/backend/search/products?sort=0&page=1&start=0&searchTerm="
	product := strings.ReplaceAll(name, " ", "%20")
	urlBlibliBehind := "&intent=true&merchantSearch=true&multiCategory=true&customUrl=&&channelId=web&showFacet=true"
	blibliCategory := ""
	if category == "Handphone" {
		blibliCategory = "&category=HA-1000002"
	} else if category == "Laptop" {
		blibliCategory = "&category=KO-1000005"
	}
	urlBlibli := urlBlibliFront + product + urlBlibliBehind + blibliCategory
	fmt.Println("URL BLIBLI : ", urlBlibli)
	data := RunScrap(urlBlibli)

	for _, item := range data.Data.Products {
		fmt.Println(item.Name)
		fmt.Println(item.Price.Price)

		saveMongo := models.ScrappingProductEcommerce{}
		saveMongo.ProductId = productId
		saveMongo.Name = item.Name
		saveMongo.Price = item.Price.Price
		saveMongo.Rating = strconv.Itoa(item.Review.Rating)
		saveMongo.Sold = item.Review.Sold
		saveMongo.Store = "Jakarta"
		saveMongo.UrlProduct = "blibli.com" + item.URL
		saveMongo.Web = "blibli"
		priceInt := int(item.Price.IntPrice)
		fmt.Println("INT BLIBLI ", priceInt)
		arrInt = append(arrInt, priceInt)

		connect := new(models.DatabaseMongo)
		ctx := context.Background()
		session, err := connect.ConnectMongoDB()
		if err != nil {
			fmt.Println("Cannot connect to MongoDB")
			// return err
		}
		defer session.Disconnect(ctx)
		var collection = session.Database("product").Collection("ecommerce_data")

		if saveMongo.Name != "" {
			findOption := options.Find()
			find, err := collection.Find(ctx, bson.M{"name": saveMongo.Name, "store": saveMongo.Store, "web": "blibli"}, findOption)
			if err != nil {
				fmt.Println("error find syntax ", find, err)
				return arrInt, err
			}
			data := []models.FindProductEcommerce{}
			err = find.All(ctx, &data)
			if err != nil {
				fmt.Println("error find all ", err)
				return arrInt, err
			}
			// fmt.Println("find ", data)
			if len(data) == 0 && saveMongo.Name != "" {
				fmt.Println("insert ", saveMongo.Name)
				insert, err := collection.InsertOne(ctx, saveMongo)
				if err != nil {
					fmt.Println("error insert ", insert, err)
					return arrInt, err
				}
			}
		}

	}
	return arrInt, nil
}

func FindMininum(values []int) (min int, e error) {
	for _, item := range values {
		fmt.Println("VALUES ", item)
	}
	if len(values) == 0 {
		return 0, errors.New("Cannot detect a minimum value in an empty slice")
	}

	min = values[0]
	for _, v := range values {
		if v < min {
			min = v
		}
	}

	return min, nil
}

func UpdateByProductId(lowestPrice int, productId string) (*mongo.UpdateResult, error) {
	filter := bson.M{"product_id": productId}
	fmt.Println("PRODUCT ID : ", productId)
	update := bson.M{"$set": bson.M{"lowest_price": lowestPrice}}
	connect := new(models.DatabaseMongo)
	ctx := context.Background()
	session, err := connect.ConnectMongoDB()
	if err != nil {
		fmt.Println("Cannot connect to MongoDB")
	}
	defer session.Disconnect(ctx)
	var collection = session.Database("product").Collection("spesificationProduct")
	res, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		fmt.Println("Cannot connect to MongoDB: Update failed ", err.Error())
	}
	defer session.Disconnect(ctx)

	if err != nil {
		return nil, err
	}
	return res, nil
}
