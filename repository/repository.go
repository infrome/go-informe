package repository

import (
	"go-informe/interfaces"
)

// repository : Repository layer
type repository struct {
}

// NewRepository : Init new repository
func NewRepository() interfaces.Repository {
	return &repository{}
}
